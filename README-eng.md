**OutilsPangloss** is a toolbox for the [**Pangloss Collection**](https://pangloss.cnrs.fr/). It allows for tailoring datasets (corpora/sub-corpora) from the collection, and doing batch downloads.

# Installation

**OutilsPangloss** is coded in Julia. Java needs to be installed on the host computer.

# Using the tool

## Tailoring datasets
All a user needs to do is to fill in a YAML file (various examples of which can be found in the `examples` folder) whose important elements are:
- the name of the language, as spelled in the Pangloss Collection (for instance: "Japhug", "Yongning Na");
- the list of graphemes (especially complex ones);
- the list of regular expressions for modifications if annotations are to be processed (deletions or rearrangements of text blocks, etc.);
- information about the corpus and sub-corpus, including:
  - the speaker filter for the sub-corpus;
  - the language of the associated summary file (French: `fr` or English: `en`) ;
  - the processing to be performed on the audio, including:
    - the sampling rate (typically `16000` Hz);
    - the depth (typically `16` bits);
    - spatialization (`separate` to separate channels into different audio files).

It is highly recommended to use one of the examples as a starting-point.

After Sparql harvesting, metadata checks (hashing, versions, etc.) and downloads, a general summary file (`data.yml`) will be found in the target folder, alongside `data` and `metadata` folders.

Note that the general summary file (`data.yml`) contains all the corpus data (including the Sparql query that was used to harvest the data and metadata) but it does not contain the data specific to the sub-corpora (such as converted audio files and duplicate annotation files).

The sub-corpora will be located in other folders, whose names have been provided among the settings described above, with other, more specific summaries tailored to sharing.

*Version française / French-language 'ReadMe': *
Ce programme sobrement intitulé **OutilsPangloss** consiste en une boîte à outils divers servant notamment à créer des (sous-)corpus de langues rares de [**la collection Pangloss**](https://pangloss.cnrs.fr/).
