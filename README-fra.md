*French version below / Version française plus bas*

# Installation

Ce programme est codé en Julia et téléchargera si besoin est un moteur XSLT 2/3 (Saxon), ainsi Java devrait aussi être installé sur l’ordinateur hôte. Du fait de sa nature, il est nécessaire d’avoir une connexion internet.

# Utilisation

## Création de corpus

Tout ce dont a besoin un utilisateur est de remplir un fichier YAML (dont différents exemples se trouvent dans le dossier `exemples`) dont les éléments importants sont les suivants :
- le nom de la langue du corpus qui se trouve sur Pangloss (« Japhug », « Yongning Na », par exemple) ;
- la liste des graphèmes (notamment complexes) ;
- la liste des expressions rationnelles de modifications si des traitements sur les annotations sont à faire (suppressions ou réarrangements de blocs de textes…) ;
- les informations sur les corpus et sous-corpus, notamment :
  - le filtre de locuteur pour le sous-corpus ;
  - la langue du fichier récapitulatif associé (français : `fr` ou anglais : `en`) ;
  - les traitements à réaliser sur l’audio, notamment :
    - le taux d’échantillonnage (typiquement `16000` Hz) ;
    - la profondeur (typiquement `16` bits) ;
    - la spatialisation (`séparer` pour séparer les canaux en différents fichiers audio).

Il est fortement conseillé de s’inspirer d’un des exemples.

Après le moissonnage Sparql, les vérifications des données par les métadonnées (hachage, versions, etc.) et les éventuels téléchargements, un fichier récapitulatif général (`données.yml`) se trouvera dans le dossier cible, aux côtés de dossiers `données` et `métadonnées`.

Notez que seul le récapitulatif général (`données.yml`) contient toutes les données du corpus (notamment la requête Sparql qui a servi à moissonner les données et métadonnées) mais qu’il ne contient pas les données propres aux sous-corpus (comme les fichiers audio convertis et les fichiers d’annotation dupliqués).

Les sous-corpus se trouveront dans d’autres dossiers dont les noms auront été paramétrés dans les informations décrites précédemment, avec d’autres récapitulatifs plus précis et adaptés au partage.
