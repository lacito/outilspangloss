FROM --platform=linux/amd64 julia:latest

RUN apt-get update
RUN apt-get install sox -y
RUN apt-get install texlive-luatex texlive-latex-recommended texlive-publishers texlive-lang-french texlive-lang-chinese -y
RUN apt-get install latexmk -y
RUN apt-get install default-jre -y
RUN apt-get install libsaxonhe-java -y
RUN apt-get install fonts-symbola fonts-sil-gentium fonts-unifont fonts-arphic-ukai -y
RUN apt-get install git -y
RUN apt-get install wget -y
RUN apt-get install zsh -y

RUN apt-get -y install locales
RUN sed -i '/fr_FR.UTF-8/s/^# //g' /etc/locale.gen && locale-gen
ENV LANG fr_FR.UTF-8
ENV LANGUAGE fr_FR:fr
ENV LC_ALL fr_FR.UTF-8

RUN useradd --create-home --shell /bin/zsh outilspangloss

WORKDIR /home/outilspangloss/.fonts

RUN wget https://github.com/CatharsisFonts/Cormorant/releases/download/v3.609/Cormorant_Install_v3.609.zip
RUN unzip -j Cormorant_Install_v3.609.zip "*.ttf"

RUN wget https://github.com/CatharsisFonts/Ysabeau/releases/download/v0.003/Ysabeau_Install_v0.003.zip
RUN unzip -j Ysabeau_Install_v0.003.zip "*.ttf"


WORKDIR /home/outilspangloss

USER outilspangloss

RUN git clone https://gitlab.com/Lacito/OutilsPangloss OutilsPangloss

RUN chown outilspangloss:outilspangloss -R *

WORKDIR /home/outilspangloss/OutilsPangloss

RUN julia --project -e "using Pkg; Pkg.instantiate(); Pkg.precompile();"

WORKDIR /home/outilspangloss/OutilsPangloss/ServeurOutilsPangloss

RUN chmod +x bin/repl
RUN chmod +x bin/server
RUN chmod +x bin/runtask

RUN julia --project -e "using Pkg; Pkg.add(url=\"..\"); Pkg.instantiate(); Pkg.precompile(); using Genie; Genie.Generator.write_secrets_file();"

EXPOSE 8000
EXPOSE 443
EXPOSE 80

ENV JULIA_DEPOT_PATH "/home/outilspangloss/.julia"
ENV GENIE_ENV "prod"
ENV HOST "0.0.0.0"
ENV PORT "8000"
ENV WSPORT "8000"
ENV JULIA_REVISE "auto"
# ENV EARLYBIND "true"

WORKDIR /home/outilspangloss/OutilsPangloss/ServeurOutilsPangloss

CMD ["bin/server"]
