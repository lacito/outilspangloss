module Outils

using ..Structures

using CSV
using DataFrames
using DataStructures: OrderedDict
using FilePaths
using LibExpat
using LightXML
using MD5
using Pipe
using TimeZones
using URIParser
using YAML

export lire, écrire, réduire_expression, réduire_lien, calculer_clef_hachage

"""
Lit un fichier (au format YAML).
"""
function lire(chemin::SystemPath)
    YAML.load_file(chemin |> string)
end

"""
Lit des données (au format YAML).
"""
function lire(données::AbstractString)
    YAML.load(données)
end

Base.Dict(structure::T) where T <: Union{ŒuvreAbstraite, InformationsLinguistiques, <:RessourceAbstraite, <:RécolteAbstraite} = Base.Dict(clef => getfield(structure, clef) for clef ∈ fieldnames(T))

OrderedDict(structure::T) where T <: Union{ŒuvreAbstraite, InformationsLinguistiques, <:RessourceAbstraite, <:RécolteAbstraite} = OrderedDict(clef => getfield(structure, clef) for clef ∈ fieldnames(T))

function YAML._print(interface::IO, corpus::Corpus, niveau::Int=0, niveau_ignoré::Bool=false)
    for (position, (clef, valeur)) ∈ corpus |> OrderedDict |> enumerate
        if clef ∉ [:chemin_local] && !isnothing(valeur)
            YAML._print(interface, (clef => valeur), niveau, position == 1)
        end
    end
end

function YAML._print(interface::IO, livret::Livret, niveau::Int=0, niveau_ignoré::Bool=false)
    for (position, (clef, valeur)) ∈ livret |> OrderedDict |> enumerate
        if clef ∉ [:chemin_local] && !isnothing(valeur)
            YAML._print(interface, (clef => valeur), niveau, position == 1)
        end
    end
end

function YAML._print(interface::IO, ressource::RessourcePatrimoineCulturel, niveau::Int=0, niveau_ignoré::Bool=false)
    for (position, (clef, valeur)) ∈ ressource |> OrderedDict |> enumerate
        YAML._print(interface, (clef => valeur), niveau, position == 1)
    end
end

function YAML._print(interface::IO, ressource::RessourceAbstraite, niveau::Int=0, niveau_ignoré::Bool=false)
    for (position, (clef, valeur)) ∈ ressource |> OrderedDict |> enumerate
        if clef ≠ :parent && !(isnothing(valeur) || (valeur isa AbstractArray && isempty(valeur)))
            YAML._print(interface, (clef => valeur), niveau, position == 1)
        end
    end
end

function YAML._print(interface::IO, récolte::RécolteAbstraite, niveau::Int=0, niveau_ignoré::Bool=false)
    YAML.print(interface, YAML._indent("\n", niveau))
    for (position, (clef, valeur)) ∈ récolte |> OrderedDict |> enumerate
        if clef ≠ :document && !isnothing(valeur)
            YAML._print(interface, (clef => valeur), niveau, false)
        end
    end
end

function YAML._print(interface::IO, informations::InformationsLinguistiques, niveau::Int=0, niveau_ignoré::Bool=false)
    YAML.print(interface, YAML._indent("\n", niveau))
    for (position, (clef, valeur)) ∈ informations |> OrderedDict |> enumerate
        if !(isnothing(valeur) || (valeur isa AbstractArray && isempty(valeur)))
            YAML._print(interface, (clef => valeur), niveau, false)
        end
    end
end

function YAML._print(interface::IO, chemin::SystemPath, niveau::Int=0, niveau_ignoré::Bool=false)
    YAML._print(interface, chemin |> string, niveau, niveau_ignoré)
end

function YAML._print(interface::IO, chemin::URI, niveau::Int=0, niveau_ignoré::Bool=false)
    YAML._print(interface, chemin |> string, niveau, niveau_ignoré)
end

function YAML._print(interface::IO, date::ZonedDateTime, niveau::Int=0, niveau_ignoré::Bool=false)
    YAML._print(interface, date |> string, niveau, niveau_ignoré)
end

function LibExpat.xpath(élément::XMLElement, modèle::LibExpat.XPath)
    LibExpat.xpath(élément |> string |> xp_parse, modèle)
end

function LightXML.XMLElement(arbre::LibExpat.ETree)
   arbre |> string |> parse_string |> root
end

Base.convert(::Type{LightXML.XMLElement}, arbre::LibExpat.ETree) = LightXML.XMLElement(arbre)

réduire(vecteur::AbstractVector) = isempty(vecteur) ? nothing : only(vecteur)
réduire_expression(vecteur::AbstractVector) = @pipe vecteur |> réduire |> (!isnothing(_) ? string(_) : nothing)
réduire_lien(vecteur::AbstractVector) = @pipe vecteur |> réduire |> (!isnothing(_) ? string(_) |> URI : nothing)


"""
Calcule la clef de hachage (MD5 par défaut) d’un fichier.
"""
function calculer_clef_hachage(chemin::SystemPath; fonction::Function=md5)::String
    résultat = chemin |> read |> fonction |> bytes2hex
    return résultat
end

end
