module OutilsPangloss

include("structures.jl")
include("outils.jl")
include("rapporteur.jl")
include("traducteur.jl")
include("audio.jl")
include("vérificateur.jl")
include("générateur.jl")
include("moissonneur.jl")
include("exporteur.jl")
include("œuvres.jl")

using .Structures
using .Outils
using .Rapporteur
using .Traducteur
using .Audio
using .Vérificateur
using .Générateur
using .Moissonneur
using .Exporteur
using .Œuvres

using FilePaths
using Pipe

export préparer_traductions, lire_configuration, créer_configuration, créer_corpus, créer_livret, lire_corpus, lire_livret

function créer_corpus(configuration::ConfigurationCorpus; retrouver::Bool=true, forcer::Bool=false)
    chemin_corpus = configuration.corpus["chemin"] |> Path
    ouvrir_rapport("Corpus $(configuration.langue)")
    corpus = if retrouver && exists(joinpath(chemin_corpus, "données.yml"))
        lire_corpus(chemin_corpus)
    else
        corpus = préparer(configuration)
        moissonner!(corpus; forcer)
        nettoyer(corpus)
    end
    for informations_souscorpus ∈ configuration.souscorpus
        critères_filtre = créer_filtre(informations_souscorpus)
        souscorpus = créer_souscorpus(
            corpus,
            critères_filtre)
        if (traitements = get(informations_souscorpus, "traitements", Dict())) |> !isempty
            chemin_souscorpus = joinpath(corpus.chemin_local, informations_souscorpus["sous-chemin"])
            générer!(
                souscorpus,
                chemin_souscorpus;
                forcer = true,
                profondeur = traitements["audio"]["profondeur"],
                taux_échantillonnage = traitements["audio"]["taux d’échantillonnage"],
                démultiplexer = traitements["audio"]["spatialisation"] == "séparer")
        end
        for récapitulatif ∈ informations_souscorpus["récapitulatifs"]
            souscorpus.nom = récapitulatif["nom complet"]
            souscorpus.commentaire = récapitulatif["commentaire"]
            écrire(souscorpus, "$(récapitulatif["nom de fichier"]).yml" |> Path; langue=récapitulatif["langue"], complet=false)
        end
        for conversion ∈ get(informations_souscorpus, "conversions", [])
            chemin_cible = joinpath(corpus.chemin_local, conversion["sous-chemin"], conversion["nom du corpus"])
            exporter(souscorpus, corpus.chemin_local, chemin_cible; format=conversion["format"])
        end
    end
    fermer_rapport()
end

function créer_livret(configuration::ConfigurationLivret; retrouver::Bool=true, forcer::Bool=false)
    chemin_livret = configuration.livret["chemin"] |> Path
    ouvrir_rapport("Livret $(configuration.langue)")
    livret = if retrouver && exists(joinpath(chemin_livret, "données.yml"))
        lire_livret(chemin_livret)
    else
        livret = préparer(configuration)
        moissonner!(livret; forcer)
        nettoyer(livret)
    end
    chemin_fichier_xml = créer_xml(livret)
    chemin_fichier_tex = @pipe splitext(chemin_fichier_xml)[1] |> "$_.tex" |> Path
    chemin_fichier_pdf = @pipe splitext(chemin_fichier_xml)[1] |> "$_.pdf" |> Path
    générer_latex(chemin_fichier_xml, chemin_fichier_tex)
    générer_pdf(chemin_fichier_tex, chemin_fichier_pdf)
    fermer_rapport()
end

function créer_filtre(informations::AbstractDict)::Function
    filtre = ressource ->
        length(ressource.enfants) == 2 &&
        (haskey(informations, "filtres") ? occursin(informations["filtres"]["locuteur"], ressource.locuteur) : true)
    return filtre
end


end
