module Moissonneur

using ..Audio
using ..Générateur
using ..Outils
using ..Rapporteur
using ..Structures

using Crayons
using DataFrames
using Downloads
using FilePaths
using HTTP
using LibExpat
using LightXML
using Mustache
using Pipe
using TimeZones
using URIParser

export moissonner!, requêter!, analyser

"""
Crée les informations linguistiques depuis un chemin.
"""
function créer_informations_linguistique(chemin::SystemPath)::InformationsLinguistiques
    if exists(chemin)
        données_linguistiques = lire(chemin)
        informations_linguistiques = InformationsLinguistiques(; graphèmes = données_linguistiques["graphèmes"])
        return informations_linguistiques
    else
        @error("Fichier d’informations linguistiques inexistant dans « $chemin ».")
    end
end

"""
Moissonne le corpus en téléchargeant puis analysant les métadonnées et les données associées, pour finalement écrire un fichier récapitulatif au format YAML.
"""
function moissonner!(corpus::Corpus, fichier_résumé::SystemPath=p"données.yml"; forcer::Bool=false)
    nombre_ressources = length(corpus.ressources)
    for (position, ressource) ∈ enumerate(corpus.ressources)
        println("Traitement de la ressource ", crayon"bold blue", position, crayon"reset", "/", crayon"bold blue", nombre_ressources, crayon"reset", " : $(ressource.identifiant).")
        moissonner!(ressource, corpus.chemin_local; forcer)
    end
    écrire(corpus, fichier_résumé)
    return corpus
end

"""
Moissonne le livret en téléchargeant puis analysant les métadonnées et les données associées.
"""
function moissonner!(livret::Livret, fichier_résumé::SystemPath=p"données.yml"; forcer::Bool=false)
    nombre_ressources = length(livret.ressources)
    for (position, ressource) ∈ enumerate(livret.ressources)
        if ressource isa RessourceAnnotationLibre
            @info("Ressource libre pas à moissonner…")
        else
            println("Traitement de la ressource ", crayon"bold blue", position, crayon"reset", "/", crayon"bold blue", nombre_ressources, crayon"reset", " : $(ressource.identifiant).")
            moissonner!(ressource, livret.chemin_local; forcer)
        end
    end
    écrire(livret, fichier_résumé)
    return livret
end

"""
Élabore une requête Sparql (à partir d’un gabarit) afin d’obtenir une récolte des premières informations sur le corpus.
"""
function requêter!(récolte::RécolteSparql, serveur::URI=uri"https://cocoon.huma-num.fr/sparql", chemin_gabarit::SystemPath=joinpath(dossier_module, "src/gabarits/moissonner.rq"); nombre_essais::Int=5)::RécolteSparql
    récolte.serveur = serveur
    informations = Dict("langue_sujet" => récolte.langue, "code_langue_locuteur" => "fr")
    récolte.requête = @pipe chemin_gabarit |> string |> render_from_file(_, informations)
    @info("requête (gabarit $chemin_gabarit) envoyée au serveur $serveur :\n$(récolte.requête)\n")
    valide = false
    numéro_essai = 1
    while !valide && numéro_essai < 5
        try
            récolte.document = @pipe récolte.requête |> HTTP.URIs.escapeuri |> HTTP.get("$serveur?query=$_", connect_timeout=20, readtimeout=20) |> getfield(_, :body) |> String |> parse_string
            valide = true
        catch erreur
            if erreur isa HTTP.TimeoutError
                @warn("Délai dépassé pour la lecture des données de la requête HTTP à l’essai numéro $numéro_essai")
            else
                error(erreur)
            end
            numéro_essai += 1
        end
    end
    return récolte
end

"""
Analyse une récolte issue d’une requête en l’analysant puis la nettoyant finement.
"""
function analyser(récolte::RécolteSparql)::Vector
    éléments = xpath(récolte.document |> root, "//result")
    données_brutes = DataFrame(
        métadonnées_patrimoine_culturel = xpath.(éléments, "binding[@name='métadonnées_patrimoine_culturel']/uri/text()") .|> réduire_lien,
        métadonnées_audio = xpath.(éléments, "binding[@name='métadonnées_audio']/uri/text()") .|> réduire_lien,
        version_audio = xpath.(éléments, "binding[@name='version_audio']/literal/text()") .|> réduire_expression,
        métadonnées_annotation = xpath.(éléments, "binding[@name='métadonnées_annotation']/uri/text()") .|> réduire_lien,
        version_annotation = xpath.(éléments, "binding[@name='version_annotation']/literal/text()") .|> réduire_expression,
        métadonnées_locuteur = xpath.(éléments, "binding[@name='métadonnées_locuteur']/uri/text()") .|> réduire_lien,
        données_audio = xpath.(éléments, "binding[@name='données_audio']/uri/text()") .|> réduire_lien,
        données_annotation = xpath.(éléments, "binding[@name='données_annotation']/uri/text()") .|> réduire_lien,
        langue = xpath.(éléments, "binding[@name='sujet']/literal/text()") .|> réduire_expression,
        code_langue = xpath.(éléments, "binding[@name='code_langue_sujet']/literal/text()") .|> réduire_expression,
        locuteur = xpath.(éléments, "binding[@name='locuteur']/literal/text()") .|> réduire_expression
    )
    données = nettoyer(données_brutes)
    données_analysées = analyser(données)
    return données_analysées
end

"""
Nettoie les données issues d’une récolte en informant des éventuelles anomalies.
"""
function nettoyer(données::DataFrame)
    codes_langue = unique(données.code_langue)
    if length(codes_langue) > 1
        @warn("Il y a plusieurs codes de langue : $(join(codes_langue, ", "))")
        rapporter(join(codes_langue, ", "), "Codes de langue multiples", 1)
    end
    groupes = groupby(données, :métadonnées_patrimoine_culturel)
    données_nettoyées = similar(données, 0)
    for groupe ∈ groupes
        locuteurs = unique(groupe.métadonnées_locuteur)
        if length(locuteurs) > 1
            @warn("Il y a plusieurs locuteurs pour la ressource patrimoniale $(first(groupe.métadonnées_patrimoine_culturel)) : $(join(locuteurs, " ; "))")
            rapporter("$(first(groupe.métadonnées_patrimoine_culturel)) : $(join(locuteurs, " ; "))", "Locuteurs multiples", 1)
        end
        push!(données_nettoyées, groupe[1,:])
    end
    return données_nettoyées
end

"""
Analyse les données issues d’une récolte en créant les ressources d’intérêt à partir des métadonnées.
"""
function analyser(données::DataFrame)::Vector{RessourcePatrimoineCulturel}
    ressources_patrimoine_culturel = RessourcePatrimoineCulturel[]
    for ligne_données ∈ eachrow(données)
        ressource_patrimoine_culturel = RessourcePatrimoineCulturel(;
            identifiant = ligne_données.métadonnées_patrimoine_culturel |> string |> splitpath |> last,
            lien_métadonnées = ligne_données.métadonnées_patrimoine_culturel,
            code_langue = ligne_données.code_langue,
            locuteur = ligne_données.locuteur)
        ressource_audio = RessourceAudioOriginale(;
            identifiant = ligne_données.métadonnées_audio |> string |> splitpath |> last,
            lien_métadonnées = ligne_données.métadonnées_audio,
            lien_données = ligne_données.données_audio,
            version = parse(Int, ligne_données.version_audio),
            parent = ressource_patrimoine_culturel)
        push!(ressource_patrimoine_culturel.enfants, ressource_audio)
        if !isnothing(ligne_données.métadonnées_annotation)
            ressource_annotation = RessourceAnnotationOriginale(;
                identifiant = ligne_données.métadonnées_annotation |> string |> splitpath |> last,
                lien_métadonnées = ligne_données.métadonnées_annotation,
                lien_données = ligne_données.données_annotation,
                version = parse(Int, ligne_données.version_annotation),
                code_langue = ligne_données.code_langue,
                parent = ressource_patrimoine_culturel)
            push!(ressource_patrimoine_culturel.enfants, ressource_annotation)
        end
        push!(ressources_patrimoine_culturel, ressource_patrimoine_culturel)
    end
    return ressources_patrimoine_culturel
end

"""
Moissonne la ressource patrimoniale en téléchargeant puis analysant les métadonnées associées, puis moissonne les ressources enfants.
"""
function moissonner!(ressource::RessourcePatrimoineCulturel, chemin_cible::SystemPath; forcer::Bool=false)::RessourcePatrimoineCulturel
    chemin_métadonnées = joinpath(chemin_cible, "métadonnées", "$(ressource.identifiant).xml")
    obtenir(ressource.lien_métadonnées, chemin_métadonnées; forcer)
    ressource.chemin_métadonnées = relpath(chemin_métadonnées, chemin_cible)
    moissonner!.(ressource.enfants, chemin_cible; forcer)
    return ressource
end

"""
Moissonne la ressource audio en téléchargeant puis analysant les métadonnées et données associées.
"""
function moissonner!(ressource::RessourceAudioOriginale, chemin_cible::SystemPath; forcer::Bool=false)::RessourceAudioOriginale
    chemin_métadonnées = joinpath(chemin_cible, "métadonnées", "$(ressource.identifiant).xml")
    obtenir(ressource.lien_métadonnées, chemin_métadonnées; forcer)
    ressource.chemin_métadonnées = relpath(chemin_métadonnées, chemin_cible)
    document_métadonnées = joinpath(chemin_cible, ressource.chemin_métadonnées) |> string |> parse_file
    version_locale = extraire_numéro_version(document_métadonnées)
    if version_locale ≠ ressource.version
        @warn("La ressource locale n’est pas à jour (version $version_locale ≠ $(ressource.version)), retéléchargement.")
        obtenir(ressource.lien_métadonnées, chemin_métadonnées; forcer=true)
    end
    ressource.clef_hachage = extraire_clef_hachage(document_métadonnées)
    chemin_données = joinpath(chemin_cible, "données", "$(ressource.identifiant).wav")
    obtenir(ressource.lien_données, chemin_données, ressource.clef_hachage; forcer)
    ressource.chemin_données = relpath(chemin_données, chemin_cible)
    (ressource.profondeur, ressource.taux_échantillonnage, ressource.spatialisation) = Audio.analyser(ressource, chemin_cible)
    return ressource
end

"""
Moissonne la ressource d’annotation en téléchargeant puis analysant les métadonnées et données associées.
"""
function moissonner!(ressource::RessourceAnnotationOriginale, chemin_cible::SystemPath; forcer::Bool=false)::RessourceAnnotationOriginale
    chemin_métadonnées = joinpath(chemin_cible, "métadonnées", "$(ressource.identifiant).xml")
    obtenir(ressource.lien_métadonnées, chemin_métadonnées; forcer)
    ressource.chemin_métadonnées = relpath(chemin_métadonnées, chemin_cible)
    document_métadonnées = joinpath(chemin_cible, ressource.chemin_métadonnées) |> string |> parse_file
    version_locale = extraire_numéro_version(document_métadonnées)
    if version_locale ≠ ressource.version
        @warn("La ressource locale n’est pas à jour (version $version_locale ≠ $(ressource.version)), retéléchargement.")
        obtenir(ressource.lien_métadonnées, chemin_métadonnées; forcer=true)
    end
    ressource.date_modification = extraire_date_modification(document_métadonnées)
    ressource.clef_hachage = extraire_clef_hachage(document_métadonnées)
    chemin_données = joinpath(chemin_cible, "données", "$(ressource.identifiant).xml")
    obtenir(ressource.lien_données, chemin_données, ressource.clef_hachage; forcer)
    ressource.chemin_données = relpath(chemin_données, chemin_cible)
    document_données = joinpath(chemin_cible, ressource.chemin_données) |> string |> parse_file
    ressource.nature = extraire_nature_annotation(document_données)
    return ressource
end

"""
Extrait la clef de hachage (si présente) d’un document XML.
"""
function extraire_clef_hachage(document::XMLDocument)::Union{String, Nothing}
    élément_clef = xpath(document |> root, "//ebucore:hashValue/text()")
    clef = if !isempty(élément_clef)
        clef = élément_clef |> only
    else
        @info("Aucune clef de hachage pour ce fichier !")
    end
    return clef
end

"""
Extrait le numéro de version d’un document XML.
"""
function extraire_numéro_version(document::XMLDocument)::Int
    version = @pipe xpath(document |> root, "//rdf:Description/schema-org:version/text()") |> only |> parse(Int, _)
    return version
end

"""
Extrait la date de modification d’un document XML.
"""
function extraire_date_modification(document::XMLDocument)::ZonedDateTime
    version = @pipe xpath(document |> root, "//rdf:Description/dcterms:modified/text()") |> only |> ZonedDateTime(_, "yyyy-mm-ddTHH:MM:SSzzzz")
    return version
end

"""
Extrait la nature d’un document XML d’annotation.
"""
function extraire_nature_annotation(document::XMLDocument)::String
    nature = (xpath(document |> root, "/*") |> only).name
    return nature
end

"""
Obtient un fichier ; s’il n’est pas déjà présent et à jour, il sera téléchargé.
"""
function obtenir(lien::URIParser.URI, chemin_cible::SystemPath, clef_hachage::Union{AbstractString, Nothing}=nothing; dossier_ressources_obsolètes::SystemPath=p"ressources obsolètes", forcer=false)::String
    chemins_candidats = [chemin_cible, insert!(chemin_cible.segments |> collect, length(chemin_cible.segments) - 1, dossier_ressources_obsolètes) |> joinpath |> Path]
    chemins_existants = SystemPath[]
    for chemin_candidat ∈ chemins_candidats
        if isfile(chemin_candidat) && stat(chemin_candidat).size ≠ 0
            if !isnothing(clef_hachage)
                clef_calculée = calculer_clef_hachage(chemin_candidat)
                if clef_hachage == clef_calculée
                    @info("Fichier « $chemin_candidat » déjà présent et conforme (par clef de hachage), il ne sera pas retéléchargé.")
                    chemin_existant = chemin_candidat
                    pushfirst!(chemins_existants, chemin_existant)
                else
                    @warn("Fichier « $chemin_candidat » déjà présent mais non conforme (par clef de hachage), il sera retéléchargé.")
                end
            else
                if !forcer
                    @info("Fichier « $chemin_candidat » déjà présent, il ne sera pas retéléchargé.")
                    chemin_existant = chemin_candidat
                    push!(chemins_existants, chemin_existant)
                else
                    @warn("Fichier « $chemin_candidat » déjà présent, mais retéléchargement demandé.")
                end
            end
        end
    end
    if !isempty(chemins_existants)
        meilleur_chemin = chemins_existants |> first
        if meilleur_chemin ≠ chemin_cible
            mv(meilleur_chemin, chemin_cible)
            @info("Déplacement du fichier « $meilleur_chemin » vers « $chemin_cible ». ")
        end
    else
        télécharger(lien, chemin_cible)
    end
    return chemin_cible
end

"""
Télécharge un fichier.
"""
function télécharger(lien::URIParser.URI, chemin_cible::SystemPath)
    format = "application/rdf+xml" |> HTTP.URIs.escapeuri
    chemin_cible |> parent |> mkpath
    @pipe "$lien?output=$format" |> string |> Downloads.download(_, chemin_cible |> string)
    @info("Téléchargement du fichier « $(chemin_cible) » (lien « $lien »).")
end

end
