<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
<xsl:output method="xml" doctype-system="https://cocoon.huma-num.fr/schemas/Archive.dtd" encoding="utf-8" indent="yes"/>
<xsl:strip-space elements="*"/>

<xsl:param name="modifications"></xsl:param>

<xsl:template match="node()|@*">
    <xsl:copy>
        <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
</xsl:template>

<xsl:template match="node()/text()">
    <xsl:variable name="texte">
        <xsl:choose>
            <xsl:when test="$modifications = ''">
                <xsl:value-of select="."/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="remplacer">
                    <xsl:with-param name="texte" select="."/>
                    <xsl:with-param name="position" select="1"/>
                    <xsl:with-param name="nom_parent" select="../name()"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:value-of select="normalize-space($texte)"/>
</xsl:template>

<xsl:template name="remplacer">
    <xsl:param name="texte"/>
    <xsl:param name="position"/>
    <xsl:param name="nom_parent"/>
    <xsl:variable name="texte_modifié" select="if ($modifications/expressions/expression[$position]/élément = $nom_parent) then replace($texte, $modifications/expressions/expression[$position]/modèle, $modifications/expressions/expression[$position]/remplacement, 'i;j') else $texte"/>
    <xsl:choose>
        <xsl:when test="$position != count($modifications/expressions/expression)">
            <xsl:call-template name="remplacer">
                <xsl:with-param name="texte" select="$texte_modifié"/>
                <xsl:with-param name="position" select="$position+1"/>
                <xsl:with-param name="nom_parent" select="$nom_parent"/>
            </xsl:call-template>
            </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="if ($modifications/expressions/expression[$position]/élément = $nom_parent) then replace($texte_modifié, $modifications/expressions/expression[$position]/modèle, $modifications/expressions/expression[$position]/remplacement, 'i;j') else $texte_modifié"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

</xsl:stylesheet>
