<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
<xsl:output method="text" encoding="utf-8" indent="yes"/>
<xsl:strip-space elements="*"/>

<xsl:param name="titre">Livret d’annotations</xsl:param>
<xsl:param name="auteur">Anonyme</xsl:param>

<xsl:template match="/">
% !TEX program = latexmk

\documentclass[11pt]{article}
\title{<xsl:value-of select="$titre"/>\\ <xsl:value-of select="Livret/Langue"/> (code \emph{<xsl:value-of select="Livret/Langue/@code"/>})}
\author{<xsl:value-of select="$auteur"/>}
\usepackage{langsci-gb4e}
\usepackage{luatexja-fontspec}
\setmainjfont{AR PL UKai CN}
\usepackage{fontspec}
\usepackage[a4paper, left=10mm, right=10mm, top=20mm, bottom=20mm]{geometry}
\usepackage[dvipsnames]{xcolor}
\usepackage{polyglossia}
\setdefaultlanguage{french}
\setmainfont{Cormorant}
\newfontfamily{\déf}[Mapping=tex-text,Ligatures=Common,Scale=MatchUppercase,Renderer=Node]{Unifont}
\newfontfamily{\api}[Mapping=tex-text,Ligatures=Common,Scale=MatchUppercase,Renderer=Node]{Gentium}
\newfontfamily{\fra}[Mapping=tex-text,Ligatures=Common,Scale=MatchUppercase]{Cormorant Garamond}
\newfontfamily{\eng}[Mapping=tex-text,Ligatures=Common,Scale=MatchUppercase]{Cormorant Upright}
\newjfontfamily{\cmn}[Mapping=tex-text,Ligatures=Common,Scale=1,Kerning=On]{AR PL UKai CN}
\newcommand{\papi}[1]{{\ltjsetparameter{jacharrange={-1, -2, -3, -4, -5, +6, +7, -8, -9}}\api #1}}
\newcommand{\pfra}[1]{{\ltjsetparameter{jacharrange={-1, -2, -3, -4, -5, +6, +7, -8, -9}}\fra #1}}
\newcommand{\peng}[1]{{\ltjsetparameter{jacharrange={-1, -2, -3, -4, -5, +6, +7, -8, -9}}\eng #1}}
\newcommand{\pcmn}[1]{{\ltjsetparameter{jacharrange={-1, +2, +3, -4, -5, +6, +7, -8, +9}}\addfontfeatures{Numbers=Lining}\cmn #1}}
\newcommand{\perreur}[1]{\papi{\textcolor{Red}{#1}}}
\usepackage{graphicx}

\begin{document}
\ltjsetparameter{jacharrange={-1, -2, -3, -4, -5, +6, +7, -8, -9}}
\setlength{\glossglue}{5pt plus 2pt minus 1pt}

<!-- \exfont{\itshape}
\glossfont{\itshape} -->
<!-- \transfont{\texttt} -->
<!-- \exnrfont{...} -->
<!-- \resetgltOffset -->
<!-- \let\eachwordone=\api
\let\eachwordtwo=\api
\let\eachwordthree=\api
\let\eachwordfor=\api -->

<xsl:apply-templates/>
\end{document}
</xsl:template>

<xsl:template match="Livret">
    <xsl:text>\maketitle</xsl:text>
    <xsl:text>&#10;&#10;</xsl:text>
    <xsl:apply-templates select="Introduction"/>
    <xsl:text>\clearpage</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:apply-templates select="Annotations"/>
</xsl:template>

<xsl:template match="Introduction">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\section{Introduction}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&#10;</xsl:text>
</xsl:template>

<xsl:template match="Annotations">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\section{Annotations}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&#10;</xsl:text>
</xsl:template>

<xsl:template match="TEXT">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\subsection{</xsl:text>
    <xsl:value-of select="HEADER/TITLE"/>
    <xsl:text> (texte </xsl:text>
    <xsl:apply-templates select="@id"/>
    <xsl:text>)}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:apply-templates select="S"/>
    <xsl:text>&#10;</xsl:text>
</xsl:template>

<xsl:template match="WORDLIST">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\subsection{</xsl:text>
    <xsl:value-of select="HEADER/TITLE"/>
    <xsl:text> (liste de mots </xsl:text>
    <xsl:apply-templates select="@id"/>
    <xsl:text>)}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:apply-templates select="W"/>
</xsl:template>

<xsl:template match="S">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{exe}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\ex </xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:apply-templates select="@id"/>
    <xsl:text>\\</xsl:text>
    <xsl:choose>
        <xsl:when test="not(W)">
            <xsl:call-template name="placer_original"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:text>&#10;</xsl:text>
            <xsl:variable name="langues_traduction" select="distinct-values(TRANSL//@xml:lang)"/>
            <xsl:call-template name="préparer_nombre_adéquat_traductions">
                <xsl:with-param name="nombre_langues" select="count($langues_traduction)"/>
            </xsl:call-template>
            <xsl:call-template name="aligner_segments_original"/>
            <xsl:variable name="arborescence" select="."/>
            <xsl:for-each select="$langues_traduction">
                <xsl:text>&#10;</xsl:text>
                <xsl:call-template name="aligner_segments_glose">
                    <xsl:with-param name="arborescence" select="$arborescence"/>
                    <xsl:with-param name="langue" select="."/>
                </xsl:call-template>
            </xsl:for-each>
        </xsl:otherwise>
    </xsl:choose>
    <xsl:if test="TRANSL">
        <xsl:text>&#10;</xsl:text>
        <xsl:if test="W">
            <xsl:text>\glt </xsl:text>
        </xsl:if>
        <xsl:call-template name="placer_traduction"/>
    </xsl:if>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{exe}</xsl:text>
    <xsl:text>&#10;</xsl:text>
</xsl:template>

<xsl:template match="W">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{exe}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\ex </xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:apply-templates select="@id"/>
    <xsl:text>\\</xsl:text>
    <xsl:call-template name="placer_original"/>
    <xsl:if test="TRANSL">
        <xsl:call-template name="placer_traduction"/>
    </xsl:if>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{exe}</xsl:text>
    <xsl:text>&#10;</xsl:text>
</xsl:template>

<xsl:template match="FORM">
    <xsl:call-template name="déterminer_langue"/>
    <xsl:text>{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="TRANSL">
    <xsl:call-template name="déterminer_langue"/>
    <xsl:text>{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="Texte[@type='introduction']">
    <xsl:text>Langue : </xsl:text>
    <xsl:value-of select="@xml:lang"/>
    <xsl:text>\\</xsl:text>
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="Texte">
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="h2">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\subsection{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="h3">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\subsubsection{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="small">
    <xsl:text>\small{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="p">
    <xsl:apply-templates/>
    <xsl:text>&#10;&#10;</xsl:text>
</xsl:template>

<xsl:template match="br">
    <xsl:text>\\ </xsl:text>
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="figure">
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\begin{figure}[h!]</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\centering</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\caption{</xsl:text>
    <xsl:apply-templates select="figcaption"/>
    <xsl:text>}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\includegraphics[width=\textwidth, height=10cm, keepaspectratio]{</xsl:text>
    <xsl:value-of select="img/@chemin_local"/>
    <xsl:text>}</xsl:text>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>\end{figure}</xsl:text>
    <xsl:text>&#10;&#10;</xsl:text>
</xsl:template>

<xsl:template match="figcaption">
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="text()">
    <xsl:value-of select="normalize-space(replace(replace(., '&quot;(.+?)&quot;', '« $1 »'), '\\', '\\textbackslash '))"/>
</xsl:template>

<xsl:template name="placer_original">
    <xsl:text>&#10;</xsl:text>
    <xsl:apply-templates select="FORM"/>
    <xsl:text>\\</xsl:text>
</xsl:template>

<xsl:template name="aligner_segments_original">
    <xsl:for-each select="W">
        <xsl:text>{</xsl:text>
        <xsl:apply-templates select="FORM"/>
        <xsl:text>}</xsl:text>
        <xsl:if test="not(position() = last())">
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:for-each>
    <xsl:text>\\</xsl:text>
</xsl:template>

<xsl:template name="aligner_segments_glose">
    <xsl:param name="arborescence"/>
    <xsl:param name="langue"/>
    <xsl:for-each select="$arborescence/W">
        <xsl:text>{</xsl:text>
        <xsl:apply-templates select="TRANSL[@xml:lang = $langue]"/>
        <xsl:text>}</xsl:text>
        <xsl:if test="not(position() = last())">
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:for-each>
    <xsl:text>\\</xsl:text>
</xsl:template>

<xsl:template name="placer_traduction">
    <xsl:for-each select="TRANSL">
        <xsl:text>‘</xsl:text>
        <xsl:apply-templates select="."/>
        <xsl:text>’</xsl:text>
        <xsl:if test="not(position() = last())">
            <xsl:text>\\</xsl:text>
            <xsl:text>&#10;</xsl:text>
        </xsl:if>
    </xsl:for-each>
</xsl:template>

<xsl:template name="déterminer_langue">
    <xsl:text>\p</xsl:text>
    <xsl:choose>
        <xsl:when test="@xml:lang = 'fr'">
            <xsl:text>fra</xsl:text>
        </xsl:when>
        <xsl:when test="@xml:lang = 'en'">
            <xsl:text>eng</xsl:text>
        </xsl:when>
        <xsl:when test="@xml:lang = 'zh'">
            <xsl:text>cmn</xsl:text>
        </xsl:when>
        <xsl:when test="@kindOf='phono'">
            <xsl:text>api</xsl:text>
        </xsl:when>
        <xsl:otherwise>
            <xsl:text>erreur</xsl:text>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="préparer_nombre_adéquat_traductions">
    <xsl:param name="nombre_langues"/>
    <xsl:text>\gl</xsl:text>
    <xsl:value-of select="nombre_langues"/>
    <xsl:for-each select="1 to $nombre_langues">
        <xsl:text>l</xsl:text>
    </xsl:for-each>
</xsl:template>

<xsl:template match="@id">
    <xsl:text>\emph{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
</xsl:template>

</xsl:stylesheet>
