module Traducteur

using ..Structures
using ..Outils

using DataStructures: OrderedDict
using FilePaths
using Glob

export préparer_traductions, traduire

function préparer_traductions(chemin::SystemPath=p"src/traductions", modèle::Glob.GlobMatch=glob"*/récapitulatif.yml")::Dict
    chemins_traductions = glob(modèle, chemin)
    global traductions = Dict(
        (chemin_traduction |> string |> splitpath)[end-1] => lire(chemin_traduction)
        for chemin_traduction ∈ chemins_traductions)
end

function traduire(élément::Any, langue::AbstractString)::Any
    if élément isa OrderedDict
        for (clef, valeur) ∈ copy(élément)
            clef_traduite = traductions[langue]["clefs"][clef]
            delete!(élément, clef)
            if valeur isa Vector
                traduire.(valeur, langue)
            elseif valeur isa OrderedDict
                traduire(valeur, langue)
            end
            if haskey(traductions[langue]["valeurs"], clef)
                valeur = traductions[langue]["valeurs"][clef][valeur]
            end
            élément[clef_traduite] = valeur
        end
    end
    return élément
end


end
