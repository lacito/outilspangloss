module Œuvres

using ..Moissonneur
using ..Outils
using ..Structures
using ..Vérificateur

using FilePaths
using Glob
using HTTP
using LibExpat
using LightXML
using Mustache
using Pipe
using TimeZones
using URIParser

export lire_configuration, créer_configuration
export lire_corpus, créer_souscorpus
export lire_livret, créer_xml, télécharger_images!
export préparer, nettoyer

"""
Lit un fichier de configuration quelconque (détecte le type selon le contenu).
"""
function lire_configuration(données_configuration::AbstractDict)::Configuration
    (est_corpus, est_livret) = haskey.(Ref(données_configuration), ["corpus", "livret"])
    if est_corpus && est_livret
        error("Fichier invalide (ambigu) : est-ce une configuration de corpus ou de livret ?")
    elseif !est_corpus && !est_livret
        error("Fichier invalide (incomplet) : est-ce une configuration de corpus ou de livret ?")
    elseif est_corpus
        return données_configuration |> lire_configuration_corpus
    else
        return données_configuration |> lire_configuration_livret
    end
end

lire_configuration(chemin_configuration::SystemPath) = chemin_configuration |> lire |> lire_configuration

lire_configuration(chemin_configuration::AbstractString) = chemin_configuration |> lire |> lire_configuration

"""
Crée un fichier de configuration minimal.
"""
function créer_configuration(données_configuration::AbstractDict, chemin_gabarit::SystemPath=joinpath(dossier_module, "src/gabarits/corpus.yml"))::Configuration
    informations = Dict(
        "langue" => données_configuration["langue"],
        "code_langue" => données_configuration["langue"]
    )
    configuration = @pipe chemin_gabarit |> string |> render_from_file(_, informations) |> lire_configuration
    return configuration
end

"""
Lit un fichier de configuration de corpus au format YAML.
"""
function lire_configuration_corpus(données_configuration::AbstractDict)::ConfigurationCorpus
    informations_linguistiques = if haskey(données_configuration, "graphèmes")
        InformationsLinguistiques(; graphèmes=données_configuration["graphèmes"]) |> vérifier
    end
    souscorpus = pop!(données_configuration["corpus"], "sous-corpus")
    configuration = ConfigurationCorpus(;
        langue = données_configuration["langue"],
        informations_linguistiques,
        modifications = get(données_configuration, "modifications", nothing),
        corpus = données_configuration["corpus"],
        souscorpus
    )
    return configuration
end

"""
Prépare un corpus à partir des informations d’un fichier de configuration en moissonnant les métadonnées à partir d’un gabarit de requête.
"""
function préparer(configuration::ConfigurationCorpus)::Corpus
    récolte = RécolteSparql(; configuration.langue)
    ressources = @pipe récolte |> requêter! |> analyser
    if isempty(ressources)
        error("Aucune ressource trouvée, veuillez si la langue $(configuration.langue) existe bien sur Pangloss !")
    end
    code_langue = [ressource.code_langue for ressource ∈ ressources] |> unique |> only
    lien_documentation = "https://pangloss.cnrs.fr/corpus/$(configuration.langue |> HTTP.URIs.escapeuri)" |> URI
    corpus = Corpus(;
        langue = configuration.langue,
        nom = configuration.corpus["nom complet"],
        commentaire = configuration.corpus["commentaire"],
        lien_documentation,
        code_langue,
        informations_linguistiques = configuration.informations_linguistiques,
        modifications = configuration.modifications,
        chemin_local = configuration.corpus["chemin"] |> Path,
        ressources,
        récolte)
    compter_ressources!(corpus)
    return corpus
end

"""
Prépare un livret à partir des informations d’un fichier de configuration en moissonnant les métadonnées à partir d’un gabarit de requête.
"""
function préparer(configuration::ConfigurationLivret)::Livret
    récolte = RécolteSparql(; configuration.langue)
    ressources = @pipe récolte |> requêter! |> analyser |> inventorier_ressources |> arranger_ressources(_, configuration.livret["ressources"])
    code_langue = [ressource.code_langue for ressource ∈ ressources] |> unique |> only
    livret = Livret(;
        langue = configuration.langue,
        nom = get(configuration.livret, "nom complet", configuration.langue),
        code_langue,
        chemin_local = configuration.livret["chemin"] |> Path,
        paramètres = configuration.livret["paramètres"],
        textes = configuration.livret["textes"],
        ressources,
        récolte
    )
    return livret
end

"""
Lit un corpus à partir d’un dossier (et son fichier résumé inclus), en lisant récursivement ses informations et ressources.
"""
function lire_corpus(chemin::SystemPath, fichier::SystemPath=p"données.yml")::Union{Corpus, Nothing}
    fichier_données = joinpath(chemin, fichier)
    if isfile(fichier_données)
        données = lire(fichier_données)
        récolte = if haskey(données, "récolte")
            RécolteSparql(;
                Dict(clef |> Symbol => valeur for (clef, valeur) ∈ données["récolte"])...)
        end
        informations_linguistiques = if haskey(données, "graphèmes")
            InformationsLinguistiques(; Dict(clef |> Symbol => valeur for (clef, valeur) ∈ données["graphèmes"])...) |> vérifier
        end
        ressources = lire_ressource.(données["ressources"])
        données = (;
            Dict(clef |> Symbol => valeur for (clef, valeur) ∈ données if clef ∉ ["ressources", "récolte"])...)
        corpus = Corpus(; données..., informations_linguistiques, ressources, récolte)
        corpus.chemin_local = chemin |> abspath
        return corpus
    else
        @error("Corpus non trouvé dans « $chemin » : fichier « $fichier_données » absent !")
    end
end

"""
Lit un livret à partir d’un dossier (et son fichier résumé inclus), en lisant récursivement ses informations et ressources.
"""
function lire_livret(chemin::SystemPath, fichier::SystemPath=p"données.yml")::Union{Livret, Nothing}
    fichier_données = joinpath(chemin, fichier)
    if isfile(fichier_données)
        données = lire(fichier_données)
        récolte = if haskey(données, "récolte")
            RécolteSparql(;
                Dict(clef |> Symbol => valeur for (clef, valeur) ∈ données["récolte"])...)
        end
        ressources = lire_ressource.(données["ressources"])
        données = (;
            Dict(clef |> Symbol => valeur for (clef, valeur) ∈ données if clef ∉ ["ressources", "récolte"])...)
        livret = Livret(; données..., ressources, récolte)
        livret.chemin_local = chemin |> abspath
        return livret
    else
        @error("Livret non trouvé dans « $chemin » : fichier « $fichier_données » absent !")
    end
end

lire_corpus(chemin::AbstractString, fichier::AbstractString) = lire_corpus(chemin |> Path, fichier |> Path)

"""
Créer un sous-corpus depuis un corpus en filtrant selon une fonction.
"""
function créer_souscorpus(corpus::Corpus, fonction::Function)::Corpus
    souscorpus = deepcopy(corpus)
    filter!(fonction, souscorpus.ressources)
    compter_ressources!(souscorpus)
    return souscorpus
end

"""
Lit un fichier de configuration de livret au format YAML.
"""
function lire_configuration_livret(données_configuration::AbstractDict)::ConfigurationLivret
    configuration = ConfigurationLivret(;
        langue = données_configuration["langue"],
        livret = données_configuration["livret"]
    )
    return configuration
end

"""
Lit récursivement une ressource dans le cadre de la lecture d’un corpus.
"""
function lire_ressource(données::Dict, parent::Union{RessourceAbstraite, Nothing}=nothing)::RessourceAbstraite
    if données["type"] == "audio original"
        ressource = RessourceAudioOriginale(;
            identifiant = données["identifiant"],
            lien_métadonnées = données["lien_métadonnées"],
            chemin_métadonnées = get(données, "chemin_métadonnées", nothing),
            lien_données = données["lien_données"],
            chemin_données = get(données, "chemin_données", nothing),
            clef_hachage = données["clef_hachage"],
            version = données["version"],
            profondeur = données["profondeur"],
            taux_échantillonnage = données["taux_échantillonnage"],
            spatialisation = données["spatialisation"],
            parent = parent)
        if haskey(données, "enfants")
            ressource.enfants = @pipe données["enfants"] .|> lire_ressource(_, ressource)
        end
    elseif données["type"] == "audio converti"
        ressource = RessourceAudioConvertie(;
            chemin_données = données["chemin_données"],
            clef_hachage = données["clef_hachage"],
            profondeur = données["profondeur"],
            taux_échantillonnage = données["taux_échantillonnage"],
            spatialisation = données["spatialisation"])
    elseif données["type"] == "annotation originale"
        ressource = RessourceAnnotationOriginale(;
            identifiant = données["identifiant"],
            lien_métadonnées = données["lien_métadonnées"],
            chemin_métadonnées = get(données, "chemin_métadonnées", nothing),
            lien_données = données["lien_données"],
            chemin_données = get(données, "chemin_données", nothing),
            code_langue = get(données, "code_langue", nothing),
            version = données["version"],
            date_modification = (@pipe données["date_modification"] |> ZonedDateTime(_, "yyyy-mm-ddTHH:MM:SSzzzz")),
            nature = données["nature"],
            parent = parent)
        if haskey(données, "segments")
            ressource.segments = données["segments"]
        end
        if haskey(données, "enfants")
            ressource.enfants = @pipe données["enfants"] .|> lire_ressource(_, ressource)
        end
    elseif données["type"] == "annotation dupliquée"
        ressource = RessourceAnnotationDupliquée(;
            chemin_données = données["chemin_données"],
            clef_hachage = données["clef_hachage"])
    elseif données["type"] == "patrimoine culturel"
        enfants = pop!(données, "enfants")
        ressource = RessourcePatrimoineCulturel(;
            identifiant = données["identifiant"],
            lien_métadonnées = données["lien_métadonnées"],
            chemin_métadonnées = get(données, "chemin_métadonnées", nothing),
            code_langue = get(données, "code_langue", nothing),
            locuteur = données["locuteur"])
        ressource.enfants = @pipe enfants .|> lire_ressource(_, ressource)
    else
        @error("Type de données ($(données["type"])) non reconnu.")
    end
    return ressource
end

"""
Compte les ressources du corpus.
"""
function compter_ressources!(corpus::Corpus)::Corpus
    corpus.nombre_patrimoines_culturels = length(corpus.ressources)
    ressources_enfants = [ressource_enfant for ressource_patrimoine_culturel ∈ corpus.ressources for ressource_enfant ∈ ressource_patrimoine_culturel.enfants]
    corpus.nombre_fichiers = isa.(ressources_enfants, RessourceAbstraite) |> count
    corpus.nombre_audios = isa.(ressources_enfants, RessourceAudioOriginale) |> count
    corpus.nombre_annotations = isa.(ressources_enfants, RessourceAnnotationOriginale) |> count
    return corpus
end

function arranger_ressources(ressources::Dict{<:AbstractString, <:RessourceAbstraite}, informations::Vector{<:Dict})::Vector{Union{RessourceAnnotationOriginale, RessourceAnnotationLibre}}
    ressources_utiles = RessourceAnnotation[]
    for information ∈ informations
        if haskey(information, "identifiant")
            ressource = get(ressources, information["identifiant"], nothing)
            if !isnothing(ressource)
                ressource.segments = get(information, "segments", nothing)
                push!(ressources_utiles, ressource)
            else
                @error("Ressource « $(information["identifiant"]) » manquante !")
            end
        elseif haskey(information, "contenu")
            @info("Ressource non répertoriée incluse en contenu libre…")
            ressource = créer_ressource_libre(information["contenu"])
            push!(ressources_utiles, ressource)
        else
            @error("Ressource malformée (ni identifiant de dépôt, ni contenu libre) !")
        end
    end
    return ressources_utiles
end

function créer_ressource_libre(contenu::AbstractString)
    chemin_données = tempname()
    write(chemin_données, contenu)
    éléments = contenu |> xp_parse
    identifiant = xpath(éléments, "/*/@id") |> only
    code_langue = xpath(éléments, "/*/@xml:lang") |> only
    ressource = RessourceAnnotationLibre(;
        identifiant,
        chemin_données,
        code_langue
    )
    return ressource
end

function créer_xml(livret::Livret; fichier::SystemPath=p"livret.xml", télécharger_images::Bool=true)::SystemPath
    document = XMLDocument()
    racine_document = create_root(document, "Livret")
    élément_langue = """<Langue code="$(livret.code_langue)">$(livret.langue)</Langue>""" |> parse_string |> root
    add_child(racine_document, élément_langue)
    élément_introduction = new_child(racine_document, "Introduction")
    for (identifiant_textes, textes) ∈ livret.textes
        for (langue_texte, texte) ∈ textes
            élément_texte = """<Texte xml:lang="$langue_texte" type="$identifiant_textes">$texte</Texte>""" |> parse_string |> root
            add_child(élément_introduction, élément_texte)
        end
    end
    élément_annotations = new_child(racine_document, "Annotations")
    for ressource ∈ livret.ressources
        racine_données = joinpath(livret.chemin_local, ressource.chemin_données) |> string |> parse_file |> root
        if hasproperty(ressource, :chemin_métadonnées)
            racine_métadonnées = joinpath(livret.chemin_local, ressource.chemin_métadonnées) |> string |> parse_file |> root
        end
        if isnothing(ressource.segments)
            add_child(élément_annotations, racine_données)
        else
            éléments_segments = récupérer_segments_ressource(racine_données, ressource.segments)
            unlink.(child_elements(racine_données))
            add_child.(Ref(racine_données), éléments_segments)
            add_child(élément_annotations, racine_données)
        end
    end
    if télécharger_images
        télécharger_images!(racine_document, livret.chemin_local)
    end
    chemin_xml = joinpath(livret.chemin_local, fichier)
    write(chemin_xml |> string, document |> string)
    return chemin_xml
end

function inventorier_ressources(ressources::Vector{<:RessourceAbstraite}, informations::AbstractDict=Dict{String, RessourceAbstraite}())
    for ressource ∈ ressources
        informations[ressource.identifiant] = ressource
        if !isempty(ressource.enfants)
            inventorier_ressources(ressource.enfants, informations)
        end
    end
    return informations
end

function récupérer_segments_ressource(élément::XMLElement, identifiants::Vector{<:AbstractString})
    éléments_utiles = XMLElement[]
    condition = join(["@id='$identifiant'" for identifiant ∈ identifiants], " or ")
    append!(éléments_utiles, xpath(élément, "/*/HEADER"))
    éléments = xpath(élément, "/WORDLIST/W[$condition]|/TEXT/S[$condition]")
    if length(éléments) ≠ length(identifiants)
        @error("Certaines ressources n’ont pas pu être trouvées…")
    end
    append!(éléments_utiles, éléments)
    return éléments_utiles
end

function récupérer_segments_ressource(élément::XMLElement, indices::Vector{<:Int})
    éléments_utiles = XMLElement[]
    append!(éléments_utiles, xpath(élément, "/*/HEADER"))
    éléments = xpath(élément, "/WORDLIST/W|/TEXT/S")
    try
        append!(éléments_utiles, éléments[indices])
    catch exception
        @error("Certaines ressources n’ont pas pu être trouvées…")
    end
    return éléments_utiles
end

function télécharger_images!(racine::XMLElement, chemin::SystemPath)
    éléments_images = trouver_éléments(racine, "img")
    for élément_image ∈ éléments_images
        lien_image = attribute(élément_image, "src")
        nom_image = @pipe lien_image |> basename |> replace(_, "_" => "") |> Path
        chemin_image = joinpath(chemin, nom_image)
        set_attribute(élément_image, "chemin_local", nom_image)
        if !isfile(chemin_image) || stat(chemin_image).size == 0
            download(lien_image, chemin_image)
        end
    end
end

function trouver_éléments(racine::XMLElement, nom::AbstractString, éléments::Vector{XMLElement}=XMLElement[])
    append!(éléments, get_elements_by_tagname(racine, nom))
    for élément ∈ child_elements(racine)
        trouver_éléments(élément, nom, éléments)
    end
    return éléments
end

"""
Nettoie le dossier cible de l’œuvre en supprimant les ressources/fichiers devenus obsolètes (typique lorsque le moissonnage s’appuie sur des ressources déjà téléchargées d’une œuvre qui a été mise à jour).
"""
function nettoyer(œuvre::ŒuvreAbstraite; dossier_ressources_obsolètes::SystemPath=p"ressources obsolètes")::ŒuvreAbstraite
    chemins_ressources_à_jour = SystemPath[]
    for ressource ∈ œuvre.ressources
        if hasproperty(ressource, :chemin_métadonnées)
            push!(chemins_ressources_à_jour, ressource.chemin_métadonnées)
        end
        if hasproperty(ressource, :chemin_données)
            push!(chemins_ressources_à_jour, ressource.chemin_données)
        end
        for ressource_enfant ∈ ressource.enfants
            push!(chemins_ressources_à_jour, ressource_enfant.chemin_métadonnées)
            push!(chemins_ressources_à_jour, ressource_enfant.chemin_données)
        end
    end
    chemins_ressources_à_jour = chemins_ressources_à_jour
    chemins_ressources_existantes = @pipe "$(œuvre.chemin_local)/*données/*" |> relpath |> glob .|> Path .|> relpath(_, œuvre.chemin_local)
    chemins_ressources_obsolètes = setdiff(chemins_ressources_existantes, chemins_ressources_à_jour)
    if !isempty(chemins_ressources_obsolètes)
        @warn("Nombre de ressources invalides ou obsolètes qui ont été déplacées dans un dossier « $dossier_ressources_obsolètes » : $(length(chemins_ressources_obsolètes)).")
        anciens_chemins = joinpath.(œuvre.chemin_local, chemins_ressources_obsolètes)
        nouveaux_chemins = joinpath.(œuvre.chemin_local, dossier_ressources_obsolètes, chemins_ressources_obsolètes)
        mkpath.(nouveaux_chemins)
        mv.(anciens_chemins, nouveaux_chemins; force=true)
    end
    return œuvre
end

end
