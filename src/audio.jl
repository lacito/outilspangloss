module Audio

using ..Structures
using FilePaths
using Glob
using Pipe

export analyser, convertir, couper

"""
Extrait les caractéstiques audio d’un document WAV (taux d’échantillonnage et profondeur).
"""
function analyser(ressource::RessourceAudioOriginale, chemin_cible::SystemPath)::Tuple
    chemin_audio = joinpath(chemin_cible, ressource.chemin_données)
    commande = `soxi $chemin_audio`
    @info("Commande : $commande")
    informations_audio = commande |> read |> String |> chomp
    spatialisation = parse(Int, match(r"Channels\s+:\s+(\d+)", informations_audio)[1])
    taux_échantillonnage = parse(Int, match(r"Sample Rate\s+:\s+(\d+)", informations_audio)[1])
    profondeur = parse(Int, match(r"Precision\s+:\s+(\d+)-bit", informations_audio)[1])
    return (profondeur, taux_échantillonnage, spatialisation)
end

function convertir(ressource::RessourceAudioOriginale, chemin_source::SystemPath, chemin_cible::SystemPath, nom::AbstractString; profondeur::Int, taux_échantillonnage::Int, démultiplexer::Bool)::Tuple
    chemins_audios_convertis = convertir_par_sox(ressource, chemin_source, chemin_cible, nom; profondeur, taux_échantillonnage, démultiplexer)
    return (chemins_audios_convertis, profondeur, taux_échantillonnage, démultiplexer)
end

function couper(chemin_source::SystemPath, début::AbstractFloat, fin::AbstractFloat, chemin_cible::SystemPath)::SystemPath
    chemins_audios_coupés = couper_par_sox(chemin_source, début, fin, chemin_cible)
    return chemins_audios_coupés
end

function convertir_par_sox(ressource::RessourceAudioOriginale, chemin_source::SystemPath, chemin_cible::SystemPath, nom::AbstractString; profondeur::Int, taux_échantillonnage::Int, démultiplexer::Bool)
    chemin_source = joinpath(chemin_source, ressource.chemin_données)
    modèle = (démultiplexer ? "$(nom)_C*.wav" : "$nom.wav")
    fichiers_préexistants = glob(modèle, chemin_cible)
    chemins_finals = SystemPath[]
    mkpath(chemin_cible)
    if isempty(fichiers_préexistants)
        if !démultiplexer
            chemin_final = joinpath(chemin_cible, "$nom.wav")
            commande = `sox -G -R --multi-threaded $chemin_source -b $profondeur -r $taux_échantillonnage $chemin_final`
            @info("Commande SoX : $commande")
            run(commande)
            push!(chemins_finals, chemin_final)
        else
            for canal ∈ 1:ressource.spatialisation
                chemin_final = joinpath(chemin_cible, "$(nom)_C$canal.wav")
                commande = `sox -G -R --multi-threaded $chemin_source -b $profondeur -r $taux_échantillonnage $chemin_final remix $canal`
                @info("Commande SoX : $commande")
                run(commande)
                push!(chemins_finals, chemin_final)
            end
        end
    else
        @info("Conversion déjà réalisée : $(join(fichiers_préexistants .|> basename, " + "))")
        chemins_finals = fichiers_préexistants
    end
    return chemins_finals
end

function couper_par_sox(chemin_source::SystemPath, début::AbstractFloat, fin::AbstractFloat, chemin_cible::SystemPath)::SystemPath
    durée = fin - début
    chemin_cible |> parent |> mkpath
    commande = `sox -G -R --multi-threaded $chemin_source $chemin_cible trim $début $durée`
    @info("Commande SoX : $commande")
    run(commande)
    return chemin_cible
end

end
