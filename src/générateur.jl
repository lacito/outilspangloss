module Générateur

using ..Audio
using ..Structures
using ..Traducteur
using ..Outils

using Crayons
using DataStructures
using FilePaths
using LightXML
using Pipe
using YAML
using ZipFile

export générer!, générer_latex, générer_pdf, écrire

"""
Génère un corpus en créant les fichiers appropriés (audios convertis et annotations dupliquées) ainsi que le récapitulatif associé.
"""
function générer!(corpus::Corpus, chemin_cible::SystemPath; forcer::Bool=false, profondeur::Int=16, taux_échantillonnage::Int=16000, démultiplexer::Bool=true, modèle_nomination::Regex=r".+(?<identifiant>cocoon-[0-9a-fA-F-]+)+")
    mkpath(chemin_cible)
    if forcer
        rm(chemin_cible; force=true, recursive=true)
    end
    ressources = Dict[]
    for ressource ∈ corpus.ressources
        couple_ressources = Dict{String, RessourceAbstraite}()
        for enfant in ressource.enfants
            if enfant isa RessourceAudioOriginale
                couple_ressources["audio"] = enfant
            elseif enfant isa RessourceAnnotationOriginale
                couple_ressources["annotation"] = enfant
            end
        end
        push!(ressources, couple_ressources)
    end
    nombre_ressources = length(ressources)
    for (position, ressource) ∈ enumerate(ressources)
        println("Traitement de la ressource ", crayon"bold blue", position, crayon"reset", "/", crayon"bold blue", nombre_ressources, crayon"reset", " : $(ressource["audio"].identifiant)")
        nom = match(modèle_nomination, ressource["audio"].identifiant)["identifiant"]
        (chemins_audios_convertis, profondeur, taux_échantillonnage, démultiplexé) = convertir(
            ressource["audio"],
            corpus.chemin_local,
            chemin_cible,
            nom;
            profondeur,
            taux_échantillonnage,
            démultiplexer)
        for chemin_audio_converti ∈ chemins_audios_convertis
            ressource_audio_convertie = RessourceAudioConvertie(;
                chemin_données = relpath(chemin_audio_converti, parent(chemin_cible)),
                clef_hachage = calculer_clef_hachage(chemin_audio_converti),
                profondeur,
                taux_échantillonnage,
                spatialisation = démultiplexé ? 1 : ressource["audio"].spatialisation)
            push!(ressource["audio"].enfants, ressource_audio_convertie)
            chemin_annotation_dupliquée = dupliquer_annotation(ressource["annotation"], corpus.chemin_local, chemin_audio_converti)
            générer_xml(chemin_annotation_dupliquée, chemin_annotation_dupliquée; modifications=corpus.modifications)
            ressource_annotation_dupliquée = RessourceAnnotationDupliquée(;
                chemin_données = relpath(chemin_annotation_dupliquée, parent(chemin_cible)),
                clef_hachage = calculer_clef_hachage(chemin_annotation_dupliquée))
            push!(ressource["annotation"].enfants, ressource_annotation_dupliquée)
        end
    end
end

"""
Duplique une annotation (normalement pour chaque fichier audio, notamment démultiplexé).
"""
function dupliquer_annotation(ressource::RessourceAnnotationOriginale, chemin_source::SystemPath, chemin_audio::SystemPath)::SystemPath
    chemin_source = joinpath(chemin_source, ressource.chemin_données)
    chemin_cible = "$(splitext(chemin_audio)[1]).xml" |> Path
    cp(chemin_source, chemin_cible; force=true)
    return chemin_cible
end

"""
Écrit l’œuvre (fichier récapitulatif) après avoir vérifié qu’elle se trouve bien au bon emplacement relativement aux ressources décrites dedans.
"""
function écrire(œuvre::ŒuvreAbstraite, chemin::SystemPath; langue::AbstractString="fr", complet::Bool=true)
    chemin_adéquat = joinpath(œuvre.chemin_local, chemin |> basename)
    if (@pipe chemin |> string |> splitpath |> filter(!isequal("."), _) |> length) == 1
        écrire(chemin_adéquat, œuvre; langue, complet)
    else
        chemin |> parent |> mkpath
        écrire(chemin, œuvre; langue, complet)
        if chemin ≠ chemin_adéquat
            @warn("Attention, ce fichier récapitulatif n’a pas été enregistré avec le chemin adéquat (« $chemin_adéquat »), les chemins relatifs internes seront sûrement brisés. C’est utile seulement à des fins de tests…")
        end
    end
end

écrire(corpus::Corpus, chemin::AbstractString; langue::AbstractString="fr", complet::Bool=true) = écrire(corpus, chemin |> Path; langue, complet)

"""
Écrit le fichier récapitulatif YAML de l’œuvre (avec traduction le cas échéant).
"""
function écrire(chemin::SystemPath, œuvre::ŒuvreAbstraite; langue::AbstractString="fr", complet::Bool=true)
    œuvre = @pipe œuvre |> YAML.write |> YAML.load(_; dicttype=OrderedDict)
    if !complet
        œuvre = nettoyer(œuvre)
    end
    if langue ≠ "fr"
        œuvre = @pipe œuvre |> traduire(_, langue)
    end
    YAML.write_file(chemin |> string, œuvre)
end

"""
Nettoie le document YAML pour supprimer les informations n’ayant pas à figurer dans le récapitulatif.
"""
function nettoyer(élément::Any, profondeur::Int=1)::Any
    if élément isa OrderedDict
        for (clef, valeur) ∈ élément
            if 2 ≤ profondeur ≤ 3 && startswith(clef, "chemin") || profondeur > 1 && clef == "code_langue" || clef == "récolte"
                delete!(élément, clef)
            end
            if valeur isa Vector
                nettoyer.(valeur, profondeur + 1)
            end
        end
    end
    return élément
end

"""
Génère un fichier XML par transformation XSL (utile pour nettoyer certains champs).
"""
function générer_xml(chemin_source::SystemPath, chemin_cible::SystemPath, chemin_gabarit::SystemPath=joinpath(dossier_module, "src/gabarits/nettoyer.xsl"); modifications::Union{AbstractVector, Nothing}=nothing)
    if !isnothing(modifications)
        chemin_modifications = créer_fichier_modifications_xml(modifications)
        générer_par_xslt(chemin_source, chemin_gabarit, chemin_cible, Dict("+modifications" => chemin_modifications))
    else
        générer_par_xslt(chemin_source, chemin_gabarit, chemin_cible)
    end
end

function générer_latex(chemin_source::SystemPath, chemin_cible::SystemPath, chemin_gabarit::SystemPath=joinpath(dossier_module, "src/gabarits/aligner.xsl"))
    générer_par_xslt(chemin_source, chemin_gabarit, chemin_cible)
end

function générer_par_xslt(chemin_source::SystemPath, chemin_style::SystemPath, chemin_cible::SystemPath, informations::AbstractDict=Dict(); nettoyer::Bool=true)::Bool
    exécutable = détecter_exécutable_xslt()
    arguments = préparer_arguments(informations)
    tampon = IOBuffer()
    message = ""
    réussite = true
    commande = `$exécutable -s:$(abspath(chemin_source)) -xsl:$(abspath(chemin_style)) -o:$(abspath(chemin_cible)) $arguments`
    @info("Commande XSLT : $commande")
    try
        @pipe commande |> Cmd(_, dir=parent(chemin_cible) |> string) |> pipeline(_; stdout=tampon, stderr=tampon) |> run
    catch exception
        réussite = false
    end
    message = tampon |> take! |> String
    if réussite
        if !isempty(message)
            chemin_journal = joinpath(parent(chemin_cible), filename(chemin_style) * ".log")
            open(chemin_journal, "w") do fichier
                write(fichier, message)
            end
            @warn("Résultat de commande XSL :\n$message\nVoir journal $chemin_journal !")
        else
            @info("Compilation XSL terminée visiblement sans erreur…")
        end
        if nettoyer
            contenu = open(chemin_cible, "r") do fichier
                fichier |> read |> String
            end
            contenu_nettoyé = contenu |> nettoyer_latex
            open(chemin_cible, "w") do fichier
                write(fichier, contenu_nettoyé)
            end
        end
    else
        @error("Erreur de compilation XSL :\n$message")
    end
    return réussite
end

function générer_pdf(chemin_source::SystemPath, chemin_cible::SystemPath; méthode::Symbol=:lualatex, forcer::Bool=false)::Bool
    exécutable = détecter_exécutable_latex(méthode)
    argument = forcer ? "-g" : ""
    commande = `$exécutable -synctex=1 -interaction=batchmode $(abspath(chemin_source)) -output-directory=$(parent(abspath(chemin_cible))) $argument`
    @info("Commande LaTeX : $commande")
    tampon = IOBuffer()
    message = ""
    réussite = true
    try
        @pipe commande |> Cmd(_, dir=parent(chemin_source) |> string) |> pipeline(_; stdout=tampon, stderr=tampon) |> run
    catch exception
        réussite = false
    end
    message = tampon |> take! |> String
    chemin_journal = joinpath(parent(chemin_cible), filename(chemin_source) * ".log")
    erreurs = open(chemin_journal, "r") do fichier
        @pipe fichier |> readlines |> filter(ligne -> occursin("Error", ligne), _)
    end
    if !réussite || !isempty(erreurs)
        compteur = counter(erreurs)
        message = @pipe ["« $erreur » ($occurrences fois)" for (erreur, occurrences) ∈ compteur] |> join(_, "\n")
        @error("Erreur de compilation LaTeX, il vaudrait peut-être mieux regarder cela de plus près…\n$message\nVoir journal $chemin_journal !")
    else
        @info("Compilation LaTeX terminée visiblement sans erreur…\nVoir journal $chemin_journal !")
        if filename(chemin_cible) ≠ filename(chemin_source)
            mv(joinpath(parent(chemin_cible), filename(chemin_source) * ".pdf"), chemin_cible; force=true)
        end
    end
    return réussite
end

function nettoyer_latex(document::AbstractString)::String
    modèle = r"(?<caractère>[\$\^_#~&])"
    substitution = s"\\\g<caractère>"
    lignes = String[]
    préambule = true
    for ligne ∈ @pipe document |> strip |> split(_, "\n")
        if !préambule
            ligne_modifiée = replace(ligne, modèle => substitution)
            # if ligne_modifiée ≠ ligne
            #     @info("Cette ligne a des caractères qui ont été protégés : $ligne_modifiée")
            # end
            push!(lignes, ligne_modifiée)
        else
            push!(lignes, ligne)
        end
        if occursin("\\begin{document}", ligne)
            préambule = false
        end
    end
    push!(lignes, "")
    document_nettoyé = join(lignes, "\n")
    return document_nettoyé
end

function détecter_exécutable_latex(méthode::Symbol)::Cmd
    latexmk_présent = if Sys.isunix()
        @pipe `whereis latexmk` |> read(_, String) |> split |> filter(!endswith(":"), _) |> !isempty
    elseif Sys.iswindows()
        try
            @pipe `where /T latexmk` |> split |> first |> parse(Int, _) |> isa(_, Number)
        catch LoadError
            false
        end
    end
    commande = latexmk_présent ? `latexmk -f -$méthode` : `$méthode`
    return commande
end

function préparer_arguments(informations::AbstractDict)::Vector{String}
    résultat = ["$clef=$valeur" for (clef, valeur) ∈ informations]
    return résultat
end

"""
Créer le fichier XML des modifications à faire (modèles, remplacements, éléments), qui sera lu par la feuille de style XSL.
"""
function créer_fichier_modifications_xml(modifications::AbstractVector)::SystemPath
    document = XMLDocument()
    racine = create_root(document, "expressions")
    for modification ∈ modifications
        élément_expression = new_child(racine, "expression")
        for (clef, valeur) ∈ modification
            élément_clef = new_child(élément_expression, clef)
            add_text(élément_clef, valeur)
        end
    end
    chemin = tempname()
    write(chemin, document |> string)
    return chemin
end

"""
Détecte l’exécutable XSLT adéquat et l’installe si nécessaire.
"""
function détecter_exécutable_xslt(exécutable_par_défaut::AbstractString="saxonb-xslt")::Cmd
    saxon_présent = if Sys.isunix()
        @pipe `whereis $exécutable_par_défaut` |> read(_, String) |> split |> filter(!endswith(":"), _) |> !isempty
    elseif Sys.iswindows()
        try
            @pipe `where /T $exécutable_par_défaut` |> split |> first |> parse(Int, _) |> isa(_, Number)
        catch LoadError
            false
        end
    end
    if saxon_présent
        commande = `$exécutable_par_défaut`
    else
        commande = installer_saxon()
    end
    return commande
end

"""
Installe Saxon (exécutable XSLT 2 & 3).
"""
function installer_saxon(lien::AbstractString="https://sourceforge.net/projects/saxon/files/Saxon-HE/10/Java/SaxonHE10-5J.zip/download", cible::SystemPath=joinpath(dossier_module, "exécutables/saxon"); forcer=false)
    if forcer || !isfile(cible) || filesize(cible) == 0
        cible |> parent |> mkpath
        lecteur = @pipe lien |> download |> ZipFile.Reader
        chemin = @pipe lecteur.files |> filter(fichier -> match(r"saxon-he-[\d.]+\.jar", fichier.name) |> !isnothing, _) |> only
        open(cible, "w") do fichier
            write(fichier, read(chemin))
        end
        close(lecteur) # Voir https://github.com/fhs/ZipFile.jl/issues/14
        chmod(cible, 0o777)
    end
    cible = cible |> abspath
    commande = `java -jar $cible`
    return commande
end

# """
# Interpole un gabarit.
# """
# function interpoler_gabarit(chemin::SystemPath, informations::Dict)::SystemPath
#     chemin_style_interpolé = tempname() |> Path
#     gabarit = read(chemin |> string, String)
#     contenu = render(gabarit, informations; tags=("�", "�"))
#     return chemin_style_interpolé
# end


end
