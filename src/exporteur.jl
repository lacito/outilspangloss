module Exporteur

using ..Audio
using ..Rapporteur
using ..Structures

using Base.Threads
using CSV
using DataFrames
using FilePaths
using LibExpat
using LightXML
using Pipe
using Random

export exporter

function exporter(corpus::Union{Corpus, Vector{<:Corpus}}, chemin_source::SystemPath, chemin_cible::SystemPath; format::AbstractString="HuggingFace-Transformers", forcer::Bool=false)
    if format ≠ "HuggingFace-Transformers"
        @error("Format « $format » non encore pris en charge…")
    else
        if forcer
            rm(chemin_cible; force=true, recursive=true)
        end
        données = exporter_pour_hft(corpus, chemin_source, chemin_cible)
        if !isempty(données)
            compresser(chemin_cible, joinpath(chemin_cible |> parent, "$(corpus.langue).zip"))
        else
            error("Corpus vide, veuillez vérifier les critères…")
        end
    end
end

function exporter_pour_hft(corpus::Corpus, chemin_source::SystemPath, chemin_cible::SystemPath; échelle_partitionnement::Symbol=:segment, traductions::Dict=Dict("entraînement" => "train", "test" => "test", "validation" => "validation"))
    données = DataFrame(
        identifiant_annotation = String[],
        chemin_annotation = SystemPath[],
        identifiant_audio = String[],
        chemin_audio = SystemPath[],
        identifiant_forme = String[],
        nature = String[],
        locuteur = String[],
        début = Float64[],
        fin = Float64[],
        forme = String[])
    extraire!(données, chemin_source, corpus)
    nettoyer!(données)
    données = segmenter!(données, chemin_cible)
    données[!, :chemin_audio] = relpath.(données.chemin_audio_segment, chemin_cible)
    @pipe chemin_cible |> parent |> joinpath(_, "données.csv") |> écrire(_, données)
    données_partitionnées = if échelle_partitionnement == :segment
        partitionner_aléatoirement_par_segments(données)
    elseif échelle_partitionnement == :fichier
        partitionner_aléatoirement_par_fichiers(données)
    else
        error("Échelle de partitionnement « $échelle_partitionnement » inconnue.")
    end
    # écrire(joinpath(chemin_cible, "total.csv"), données_partitionnées[:, r"^(chemin_audio|nature|forme|traduction.+|partition)"])
    for ((partition,), groupe) ∈ groupby(données_partitionnées, :partition) |> pairs
        écrire(joinpath(chemin_cible, "$(traductions[partition]).csv"), groupe[:, r"^(chemin_audio|nature|locuteur|forme|traduction.+)"])
    end
    return données_partitionnées
end

function extraire!(données::DataFrame, chemin_source::SystemPath, corpus::Corpus)
    extraire!.(Ref(données), chemin_source, corpus.ressources)
end

function extraire!(données::DataFrame, chemin_source::SystemPath, ressource::RessourcePatrimoineCulturel)
    if Set(ressource.enfants .|> typeof) == Set([RessourceAnnotationOriginale, RessourceAudioOriginale])
        if length(ressource.enfants) ≠ 2
            @error("Nombre incorrect de ressources enfants : $(ressource.enfants).")
            rapporter("$(ressource.enfants) au lieu de 2", "Nombre incorrect de ressources enfants", 1)
        else
            informations_annotations = extraire.(chemin_source, @pipe ressource.enfants |> filter(enfant -> isa(enfant, RessourceAnnotationOriginale), _))
            informations_audios = extraire.(chemin_source, @pipe ressource.enfants |> filter(enfant -> isa(enfant, RessourceAudioOriginale), _))
            for (informations_annotation, informations_audio) ∈ zip(informations_annotations, informations_audios)
                informations_audio = repeat(informations_audio, size(informations_annotation, 1))
                informations = hcat(informations_annotation, informations_audio)
                informations[!,:locuteur] .= ressource.locuteur
                append!(données, informations; cols=:union)
            end
        end
    end
end

function extraire(chemin_source::SystemPath, ressource::RessourceAudioOriginale)
    chemin_données = ressource.enfants |> isempty ? ressource.chemin_données : (ressource.enfants |> first).chemin_données
    @info(chemin_données)
    informations = DataFrame(
        identifiant_audio = chemin_données |> basename,
        chemin_audio  = joinpath(chemin_source, chemin_données)
    )
    return informations
end

function extraire(chemin_source::SystemPath, ressource::RessourceAnnotationOriginale)::DataFrame
    ouvrir_rapport("ressource $(ressource.identifiant)"; surcontextualiser=true)
    @info(chemin_source)
    chemin_données = ressource.enfants |> isempty ? ressource.chemin_données : (ressource.enfants |> first).chemin_données
    @info(chemin_données)
    document = joinpath(chemin_source, chemin_données) |> string |> parse_file
    contenu = extraire_informations(document)
    langues = [([traductions |> keys for traductions ∈ contenu["traductions"]]...)...] |> Set
    colonnes_langues = @pipe langues |> "traduction:" .* _
    traductions = [[get(traduction, langue, missing) for traduction ∈ contenu["traductions"]] for langue ∈ langues]
    if isempty(contenu["forme"]) && !isempty(contenu["identifiant"])
        contenu["forme"] = repeat([missing], length(contenu["identifiant"]))
    end
    informations = try
        informations = DataFrame(
            identifiant_annotation = chemin_données |> basename,
            chemin_annotation  = chemin_données,
            identifiant_forme = contenu["identifiant"],
            nature = ressource.nature,
            début = contenu["début"],
            fin = contenu["fin"],
            forme = contenu["forme"]
        )
        insertcols!.(Ref(informations), colonnes_langues .=> traductions)
        informations
    catch erreur
        if erreur isa DimensionMismatch
            @error("Fichier non pris en compte, erreur irrécupérable automatiquement…")
            rapporter(chemin_source, "Fichier non pris en compte (erreur de dimensions)", 1)
        end
        prinln(erreur)
    end
    fermer_rapport()
    return informations
end

function extraire_informations(document::XMLDocument; modèles_forme_préférés::Vector{<:AbstractString} = ["phonemic", "phono", "ortho", ""], séparateur_concaténation::AbstractString=" ; ")::Dict
    informations = Dict(
        "identifiant" => String[],
        "début" => Union{Float16, Missing}[],
        "fin" => Union{Float16, Missing}[],
        "forme" => Union{String, Missing}[],
        "traductions" => Dict[]
    )
    éléments = @pipe xpath(document |> root, "//AUDIO/..")
    for élément ∈ éléments
        identifiant = xpath(élément, "@id") |> only
        début = @pipe xpath(élément, "AUDIO/@start") |> only
        fin = @pipe xpath(élément, "AUDIO/@end") |> only
        début = if !isempty(début)
            parse(Float64, début)
        else
            @error("Chronocode de début manquant.")
            rapporter("segment $identifiant", "Chronocode de début manquant.", 1)
            missing
        end
        fin = if !isempty(fin)
            parse(Float64, fin)
        else
            @error("Chronocode de fin manquant.")
            rapporter("segment $identifiant", "Chronocode de fin manquant.", 1)
            missing
        end
        langues = @pipe xpath(élément, "TRANSL") .|> xpath(_, "@xml:lang") .|> join(_)
        traductions = @pipe xpath(élément, "TRANSL") .|> getproperty(_, :elements) .|> join(_)
        langues_traductions = Dict{String, String}()
        for (traduction, langue) ∈ zip(traductions, langues)
            traduction_déjà_présente = get(langues_traductions, langue, missing)
            if !ismissing(traduction_déjà_présente)
                @error("Il y a plusieurs traductions de même langue « $langue » pour l’élément « $identifiant ». Elles seront concaténées avec « $séparateur_concaténation ».")
                rapporter("segment $identifiant : $langue", "Traductions de même langue multiples", 1)
                langues_traductions[langue] = join([traduction_déjà_présente, traduction], séparateur_concaténation)
            else
                langues_traductions[langue] = traduction
            end
        end
        éléments_formes = xpath(élément, "FORM")
        formes = @pipe éléments_formes .|> getproperty(_, :elements) .|> join(_)
        attributs = @pipe éléments_formes .|> getproperty(_, :attr)
        attributs_formes = Dict{String, String}()
        for (forme, attribut) ∈ zip(formes, attributs)
            type = get(attribut, "kindOf", "")
            forme_déjà_présente = get(attributs_formes, type, nothing)
            if !isnothing(forme_déjà_présente)
                @error("Il y a plusieurs formes de même type « $type » pour l’élément « $identifiant ». Elles seront concaténées avec « $séparateur_concaténation ».")
                rapporter("segment $identifiant : $type", "Formes de même type multiples", 1)
                attributs_formes[type] = join([forme_déjà_présente, forme], séparateur_concaténation)
            else
                attributs_formes[type] = forme
            end
        end
        forme = missing
        for modèle_forme ∈ modèles_forme_préférés
            forme = get(attributs_formes, modèle_forme, missing)
            if !ismissing(forme)
                @info("Forme « $modèle_forme » trouvée : $forme.")
                break
            end
        end
        if isempty(attributs_formes)
            @error("Il n’y a aucune forme pour l’élément « $identifiant ».")
            rapporter("segment $identifiant", "Forme absente", 2)
        elseif ismissing(forme)
            @error("Il y a une forme manquante pour l’élément « $identifiant ».")
            rapporter("segment $identifiant", "Forme nulle", 2)
        end
        push!(informations["identifiant"], identifiant)
        push!(informations["début"], début)
        push!(informations["fin"], fin)
        push!(informations["forme"], forme)
        push!(informations["traductions"], langues_traductions)
    end
    return informations
end

function nettoyer!(données::DataFrame)
    condition_validité = ligne -> [ligne["forme"], ligne["début"], ligne["fin"]] .|> !ismissing |> all
    lignes_invalides = filter(!condition_validité, données)
    filter!(condition_validité, données)
    @warn("Les $(size(lignes_invalides, 1)) lignes invalides suivantes ont été supprimées des données avant l’exportation :\n$lignes_invalides")
end

function segmenter!(données::DataFrame, chemin_cible::SystemPath)::DataFrame
    données[!, :chemin_audio_segment] = joinpath.(chemin_cible, données.identifiant_audio .|> splitext .|> first, données.identifiant_forme .* ".wav")
    données.chemin_audio_segment .|> parent .|> mkpath
    @threads for segment ∈ eachrow(données)
        couper(segment.chemin_audio, segment.début, segment.fin, segment.chemin_audio_segment)
    end
    return données
end

function partitionner_aléatoirement_par_segments(données::DataFrame)::DataFrame
    taille_totale = size(données, 1)
    partitions = déterminer_partitions(taille_totale)
    données_permutées = données[shuffle(axes(données, 1)), :]
    données_partitionnées = hcat(données_permutées, DataFrame(partition=partitions))
    return données_partitionnées
end

function partitionner_aléatoirement_par_fichiers(données::DataFrame)::DataFrame
    groupes = groupby(données, :identifiant_annotation)
    taille_totale = size(groupes, 1)
    partitions = déterminer_partitions(taille_totale)
    groupes_permutés = groupes[shuffle(axes(groupes, 1))]
    groupes_partitionnés = DataFrame[]
    for (groupe, partition) ∈ zip(groupes_permutés, partitions)
        groupe_partitionné = hcat(groupe, DataFrame(partition=repeat([partition], size(groupe, 1))))
        push!(groupes_partitionnés, groupe_partitionné)
    end
    données_partitionnées = vcat(groupes_partitionnés...)
    return données_partitionnées
end

function déterminer_partitions(taille_totale::Int; partitionnement::Dict{String, Float64} = Dict("entraînement" => 0.8, "test" => 0.1, "validation" => 0.1))::Vector
    @assert partitionnement |> values |> sum == 1
    tailles = @pipe taille_totale .* values(partitionnement) .|> round .|> Int |> collect
    if (nouvelle_taille_totale = tailles |> sum) ≠ taille_totale
        tailles[1] -= nouvelle_taille_totale - taille_totale
    end
    partitions = @pipe [repeat([nom_partition], valeur) for (nom_partition, valeur) ∈ zip(keys(partitionnement), tailles)] |> vcat(_...)
    return partitions
end

function compresser(chemin_source::SystemPath, chemin_cible::SystemPath; méthode::AbstractString="bzip2")
    commande = Cmd(`zip -q -r -9 -Z $méthode $(chemin_cible |> absolute) $(chemin_source |> last)`; dir=chemin_source |> parent |> absolute |> string)
    @info("Commande Zip : $commande")
    run(commande)
end

"""
Écrit un fichier (au format CSV).
"""
function écrire(chemin::SystemPath, données::AbstractDataFrame)
    chemin |> parent |> mkpath
    données |> CSV.write(chemin)
end

écrire(chemin::AbstractString, données::AbstractDataFrame) = données |> CSV.write(chemin |> Path)

end
