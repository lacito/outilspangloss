module Vérificateur

using ..Structures
using DataStructures

export vérifier

"""
Vérifie la cohérence globale des informations linguistiques (unicité et cohérence des graphèmes, etc.).
"""
function vérifier(informations::InformationsLinguistiques)
    graphèmes_liste_dupliqués, graphèmes_liste_nettoyés = informations.graphèmes |> vérifier_unicité
    if !isempty(graphèmes_liste_dupliqués)
        @warn("Les graphèmes suivants (de la liste) étaient dupliqués et ont été nettoyés : $(join(graphèmes_liste_dupliqués, ", "))")
    end
    informations.graphèmes = graphèmes_liste_nettoyés
    return informations
end

"""
Vérifie l’unicité des graphèmes au sein d’un vecteur.
"""
function vérifier_unicité(graphèmes::Vector)
    graphèmes_nettoyés = OrderedSet{String}()
    graphèmes_dupliqués = OrderedSet{String}()
    for graphème ∈ graphèmes
        if graphème ∉ graphèmes_nettoyés
            push!(graphèmes_nettoyés, graphème)
        else
            push!(graphèmes_dupliqués, graphème)
        end
    end
    return (graphèmes_dupliqués |> collect, graphèmes_nettoyés |> collect)
end

"""
Aplatit un vecteur, notamment quand d’autres vecteurs y sont inclus.
"""
function aplatir(vecteur::Vector, éléments::Vector=[])
    for élément ∈ vecteur
        if élément isa Vector
            aplatir(élément, éléments)
        else
            push!(éléments, élément)
        end
    end
    return éléments
end

end
