module Structures

using FilePaths
using LightXML
using Parameters
using TimeZones
using URIParser

export Configuration, RécolteAbstraite, RessourceAbstraite, ŒuvreAbstraite
export InformationsLinguistiques, ConfigurationCorpus, ConfigurationLivret, RécolteSparql, RessourcePatrimoineCulturel, RessourceAudio, RessourceAudioOriginale, RessourceAudioConvertie, RessourceAnnotation, RessourceAnnotationOriginale, RessourceAnnotationDupliquée, RessourceAnnotationLibre, Corpus, Livret
export @uri_str
export dossier_module

global dossier_module = Path(@__FILE__) |> dirname |> parent

macro uri_str(chemin)
    return :(URI($chemin))
end

abstract type Configuration end
abstract type RécolteAbstraite end
abstract type RessourceAbstraite end
abstract type RessourceAudio <: RessourceAbstraite end
abstract type RessourceAnnotation <: RessourceAbstraite end
abstract type ŒuvreAbstraite end

@with_kw mutable struct InformationsLinguistiques
    graphèmes::Vector{<:String}
end

@with_kw struct ConfigurationCorpus <: Configuration
    langue::String
    informations_linguistiques::Union{InformationsLinguistiques, Nothing} = nothing
    modifications::Union{Vector{<:Dict}, Nothing} = nothing
    corpus::Dict
    souscorpus::Vector{<:Dict}
end

@with_kw struct ConfigurationLivret <: Configuration
    langue::String
    livret::Dict
end

@with_kw mutable struct RécolteSparql <: RécolteAbstraite
    langue::String
    serveur::Union{URI, Nothing} = nothing
    requête::Union{String, Nothing} = nothing
    document::Union{XMLDocument, Nothing} = nothing
end

@with_kw mutable struct RessourcePatrimoineCulturel <: RessourceAbstraite
    identifiant::String
    type::String = "patrimoine culturel"
    lien_métadonnées::URI
    chemin_métadonnées::Union{SystemPath, Nothing} = nothing
    code_langue::Union{String, Nothing} = nothing
    locuteur::Union{String, Vector{<:String}}
    enfants::Vector{<:RessourceAbstraite} = RessourceAbstraite[]
end

@with_kw mutable struct RessourceAudioConvertie <: RessourceAudio
    type::String = "audio converti"
    chemin_données::SystemPath
    clef_hachage::AbstractString
    profondeur::Int
    taux_échantillonnage::Int
    spatialisation::Int
end

@with_kw mutable struct RessourceAudioOriginale <: RessourceAudio
    identifiant::String
    type::String = "audio original"
    lien_métadonnées::URI
    chemin_métadonnées::Union{SystemPath, Nothing} = nothing
    lien_données::URI
    chemin_données::Union{SystemPath, Nothing} = nothing
    clef_hachage::Union{AbstractString, Nothing} = nothing
    version::Union{Int, Nothing} = nothing
    profondeur::Union{Int, Nothing} = nothing
    taux_échantillonnage::Union{Int, Nothing} = nothing
    spatialisation::Union{Int, Nothing} = nothing
    parent::Union{RessourcePatrimoineCulturel, Nothing}
    enfants::Vector{<:RessourceAudioConvertie} = RessourceAudioConvertie[]
end

@with_kw mutable struct RessourceAnnotationDupliquée <: RessourceAnnotation
    type::String = "annotation dupliquée"
    chemin_données::SystemPath
    clef_hachage::AbstractString
end

@with_kw mutable struct RessourceAnnotationOriginale <: RessourceAnnotation
    identifiant::String
    type::String = "annotation originale"
    lien_métadonnées::URI
    chemin_métadonnées::Union{SystemPath, Nothing} = nothing
    lien_données::URI
    chemin_données::Union{SystemPath, Nothing} = nothing
    code_langue::Union{String, Nothing} = nothing
    clef_hachage::Union{AbstractString, Nothing} = nothing
    version::Union{Int, Nothing} = nothing
    date_modification::Union{ZonedDateTime, Nothing} = nothing
    nature::Union{String, Nothing} = nothing
    segments::Union{Vector, Nothing} = nothing
    parent::Union{RessourcePatrimoineCulturel, Nothing}
    enfants::Vector{<:RessourceAnnotationDupliquée} = RessourceAnnotationDupliquée[]
end

@with_kw mutable struct RessourceAnnotationLibre <: RessourceAnnotation
    identifiant::String
    type::String = "annotation libre"
    chemin_données::Union{SystemPath, Nothing} = nothing
    code_langue::Union{String, Nothing} = nothing
    clef_hachage::Union{AbstractString, Nothing} = nothing
    version::Union{Int, Nothing} = nothing
    date_modification::Union{ZonedDateTime, Nothing} = nothing
    nature::Union{String, Nothing} = nothing
    segments::Union{Vector, Nothing} = nothing
    enfants::Vector{<:RessourceAnnotationDupliquée} = RessourceAnnotationDupliquée[]
end

@with_kw mutable struct Corpus <: ŒuvreAbstraite
    langue::String
    nom::String
    commentaire::String = ""
    lien_documentation::URI
    code_langue::String
    version::String = "1.0"
    informations_linguistiques::Union{InformationsLinguistiques, Nothing} = nothing
    modifications::Union{Vector{<:Dict}, Nothing} = nothing
    chemin_local::SystemPath = p"."
    nombre_patrimoines_culturels::Int = 0
    nombre_fichiers::Int = 0
    nombre_audios::Int = 0
    nombre_annotations::Int = 0
    ressources::Vector{<:RessourceAbstraite} = RessourceAbstraite[]
    récolte::Union{RécolteAbstraite, Nothing} = nothing
end

@with_kw mutable struct Livret <: ŒuvreAbstraite
    langue::String
    nom::String
    code_langue::String
    chemin_local::SystemPath = p"."
    paramètres::Dict
    textes::Dict
    ressources::Vector{Union{RessourceAnnotationOriginale, RessourceAnnotationLibre}} = Union{RessourceAnnotationOriginale, RessourceAnnotationLibre}[]
    récolte::Union{RécolteAbstraite, Nothing} = nothing
end

Base.broadcastable(structure::T) where T <: RécolteAbstraite = Ref(structure)

Base.Dict(structure::T) where T <: RécolteAbstraite = Base.Dict(clef => getfield(structure, clef) for clef ∈ fieldnames(T))

Base.convert(::Type{VersionNumber}, expression::AbstractString) = VersionNumber(expression)
# Base.convert(::Type{Symbol}, expression::AbstractString) = Symbol(expression)
Base.convert(::Type{URI}, expression::AbstractString) = URI(expression)
Base.convert(::Type{Symbol}, expression::AbstractString) = Symbol(expression)
Base.convert(::Type{SystemPath}, expression::AbstractString) = Path(expression)

end
