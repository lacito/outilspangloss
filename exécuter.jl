using OutilsPangloss

using ArgMacros
using FilePaths
using Glob
using OutilsPangloss.Rapporteur

function exécuter(chemin_configuration::Union{SystemPath, AbstractString, Nothing}=nothing, type::AbstractString="corpus"; retrouver::Bool=false, forcer::Bool=false)
    if !isinteractive()
        @inlinearguments begin
            @helpusage "julia --project exécuter.jl [--corpus/--livret] langue/chemin_fichier_configuration.yml"
            @helpdescription "Lance OutilsPangloss afin de créer un corpus (choix par défaut) ou un livret."
            @argumentflag argument_corpus "--corpus"
            @arghelp "Crée un corpus."
            @argumentflag argument_livret "--livret"
            @arghelp "Crée un livret."
            @argumentflag argument_retrouver "--retrouver"
            @arghelp "Tente de retrouver des données locales plutôt que télécharger."
            @argumentflag argument_forcer "--forcer"
            @arghelp "Force le retéléchargement des données (si hachage invalide le cas échéant)."
            @positionalrequired String argument_configuration "chemin d’accès du fichier de configuration YAML ou langue Pangloss seule"
            @arghelp "Précise le dossier du fichier de configuration ou le nom complet de la langue Pangloss."
        end
        type = argument_livret ? "livret" : "corpus"
    else
        argument_configuration = chemin_configuration
        argument_retrouver = retrouver
        argument_forcer = forcer
    end
    if splitext(argument_configuration)[end] == ".yml" && isfile(argument_configuration)
        configuration = argument_configuration |> Path |> lire_configuration
    else
        configuration = Dict("type" => type, "langue" => argument_configuration) |> créer_configuration
    end
    préparer_traductions()
    créer_rapporteur()
    if type == "corpus"
        créer_corpus(configuration; retrouver=argument_retrouver, forcer=argument_forcer)
    else
        créer_livret(configuration; retrouver=argument_retrouver, forcer=argument_forcer)
    end
    @time générer_rapport("rapport $type $(configuration.langue)" |> Path)
end

if !isinteractive()
    exécuter()
else
    chemin_corpus_exemples = "exemples/corpus *.yml"
    chemins_configurations_corpus = glob(chemin_corpus_exemples) .|> Path
    for chemin ∈ chemins_configurations_corpus
        @info("Corpus « $chemin » en cours…")
        exécuter(chemin, "corpus", retrouver=false)
    end

    chemin_livrets_exemples = "exemples/livret *.yml"
    chemins_configurations_livrets = glob(chemin_livrets_exemples) .|> Path
    for chemin ∈ chemins_configurations_livrets
        @info("Livret « $chemin » en cours…")
        exécuter(chemin, "livret", retrouver=false)
    end
end
