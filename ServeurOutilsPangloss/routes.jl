
using Genie
using ServeurOutilsPangloss.Livret

route("/") do
  serve_static_file("welcome.html")
end

route("/api/livret", demander_livret; method=POST)
route("/api/test", tester_demande_livret; method=GET)
route("/api/testjson", tester_demande_livret_json; method=GET)
