module Livret

using Genie
using Genie.Renderer.Json
using Genie.Requests
using Genie.Router
using OutilsPangloss
using OutilsPangloss.Rapporteur
using OutilsPangloss.Structures
using Dates
using FilePaths
using Glob
using HTTP
using JSON3
using YAML
using ZipFile

export demander_livret, tester_demande_livret, tester_demande_livret_json

function créer_dossiers_temporaires(identifiant::AbstractString)::Dict
    identifiant = "outilspangloss-$identifiant"
    dossiers = Dict(
        "travail" => joinpath(tempdir(), identifiant) |> Path,
        # "polices" => joinpath(homedir(), ".fonts", identifiant) |> Path,
        "résultats" => joinpath("public/temporaire", identifiant) |> Path
    )
    dossiers |> values .|> mkpath
    return dossiers
end

function demander_livret()
    cargaison = Dict{String, Any}(jsonpayload())
    erreur = ""
    jeton = cargaison["jeton"]
    @info("Jeton = $jeton")
    chemins_dossiers = créer_dossiers_temporaires(cargaison["identifiant"])
    configuration = JSON3.read(string(cargaison["configuration"]), Dict{String, Any})
    println("+++", configuration)
    configuration["livret"]["chemin"] = chemins_dossiers["travail"]
    chemin_configuration = joinpath(chemins_dossiers["travail"], "configuration.yml")
    YAML.write_file(chemin_configuration |> string, configuration)
    statut = "correct"
    erreur = false
    chemin_archive = nothing
    préparer_traductions()
    configuration = reconvertir_configuration(configuration)
    créer_rapporteur()
    créer_livret(configuration, retrouver=false)
    # catch exception
    #     @error(exception)
    #     statut = "erreur"
    #     erreur = exception |> string
    # end
    if !isempty(erreur)
        @warn(readdir(chemins_dossiers["travail"]))
        chemins = vcat([glob("*.$extension", chemins_dossiers["travail"]) for extension ∈ ["pdf", "tex", "log"]]...) .|> Path
        chemin_archive = préparer_résultats(chemins, chemins_dossiers["travail"], chemins_dossiers["résultats"])
    end
    # chemin_public = "$(Genie.config.server_host):$(Genie.config.server_port)/$chemin_archive"
    chemin_public = chemin_archive
    résultat = Dict(
        "statut" => statut,
        "erreur" => erreur,
        "chemin" => chemin_public
    )
    résultat |> JSON3.write
end

function tester_demande_livret()
    fichier = "../exemples/livret japhug.yml"
    configuration = fichier |> YAML.load_file
    contenu = Dict(
        "jeton" => "clef secrète !",
        "identifiant" => "japhug-001",
        "configuration" => configuration
    ) |> JSON3.write
    requête = HTTP.request("POST", "http://$(Genie.config.server_host):$(Genie.config.server_port)/api/livret", [("Content-Type", "application/json")], contenu)
    requête.body |> String |> JSON3.read |> json
end

function tester_demande_livret_json()
    fichier = "../exemples/livret nepali test.json"
    configuration = JSON3.read(fichier |> read, Dict)
    contenu = Dict(
        "jeton" => "clef secrète !",
        "identifiant" => "nepali-001",
        "configuration" => configuration
    ) |> JSON3.write
    open("contenu.json", "w") do fichier
        write(fichier, contenu)
    end
    requête = HTTP.request("POST", "http://$(Genie.config.server_host):$(Genie.config.server_port)/api/livret", [("Content-Type", "application/json")], contenu)
    requête.body |> String |> JSON3.read |> json
end

function récupérer_chemins_résultats(données::Dict, chemin_dossier::SystemPath, nom_journal::AbstractString)::Vector{<:SystemPath}
    informations = YAML.load_file(données["fichiers"]["configuration"] |> string)
    préchemins = [
        informations["chemins"]["xml"] |> Path,
        (informations["chemins"]["latex"] .|> Path)...,
        joinpath.(informations["chemins"]["pdf"] .|> Path .|> parent |> Set, "*")...,
        nom_journal
    ]
    chemins = vcat((joinpath.(relpath(chemin_dossier), préchemins) .|> string .|> glob)...) .|> abspath .|> Path
    filter!(exists, chemins)
    return chemins
end

function préparer_résultats(chemins_fichiers::Vector{<:SystemPath}, chemin_source::SystemPath, chemin_cible::SystemPath)
    chemin_archive = joinpath(chemin_source, "$(now()).zip" |> string)
    écriveur = ZipFile.Writer(chemin_archive |> string)
    for chemin_fichier ∈ chemins_fichiers
        contenu = open(chemin_fichier, "r") do fichier
            read(fichier, String)
        end
        archive = ZipFile.addfile(écriveur, basename(chemin_fichier))
        write(archive, contenu)
    end
    close(écriveur)
    fichier_résultat = basename(chemin_archive)
    destination = joinpath(chemin_cible, fichier_résultat)
    destination |> parent |> mkpath
    symlink(chemin_archive, destination)
    destination_en_ligne = splitpath(destination |> string)[2:end] |> joinpath
    return destination_en_ligne
end

function reconvertir_configuration(configuration::AbstractDict)::ConfigurationLivret
    configuration = configuration |> lire_configuration
    configuration.livret["ressources"] = configuration.livret["ressources"] |> Vector{Dict}
    for ressource ∈ configuration.livret["ressources"]
        if haskey(ressource, "segments")
            ressource["segments"] = ressource["segments"] |> Vector{typeof.(ressource["segments"]) |> unique |> only}
        end
    end
    return configuration
end

end
